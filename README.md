# QJSTemplateEngine - A Template Engine for Qt utilizing JavaScript

QJSTemplateEngine is a C++ template engine which is built on top of Qt. It reads templates which
are formatted in a language similar to e.g. PHP and transpiles them to JavaScript. These
JavaScripts are then executed in Qt's QJSEngine in order to generate arbitratry output.

## Requirements

QJSTemplateEngine is directly built on top of Qt 5.5 (and up). It depends on Qt's Core module as
well as the QML module (which provides the QJSEngine class which is used to executed
transpiled JavaScript code).

## Usage

There are two ways to use QJSTemplateEngine:

### Using libqjstemplateengine

You can use the QJSTemplateEngine library (libqjstemplateengine) directly in your application. This
allows you to directly generate output without external dependencies. This is useful if your
application wants to use the library for generating e.g. parts of its UI (which might
be interesting especially if you write some kind of service which runs in the background and
only provides a user interface via an HTTP front end).

### Using the qjsgen command line tool

QJSTemplateEngine comes with the command line tool *qjsgen*. This is a generic code generator,
which can be passed into template files and arbitrary data files and which generated appropriate
content out of it. You can use this tool if your data is formatted in one of the
supported standard input formats and no **tight** integration into an application is required
(e.g. in case you want to use some code generation in a build system).

## License

QJSTemplateEngine is licensed under the terms of the GNU Lesser General Public License, either
version 3 or (at your option) any later version.
