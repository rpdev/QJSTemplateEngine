#ifndef QJSTEMPLATEENGINE_ENGINE_P_H
#define QJSTEMPLATEENGINE_ENGINE_P_H

#include <QDebug>
#include <QString>
#include <QHash>

#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/DataProvider"

namespace QJSTemplateEngine {

struct EnginePrivate {

  Engine::EngineError error;
  QString errorString;
  SharedConfiguration configuration;
  QHash<QString,DataProvider*> dataProviders;
  QHash<QString,DataProvider*> files;

  EnginePrivate() :
    error( Engine::NoError ),
    errorString(),
    configuration( new Configuration() ),
    dataProviders() {
  }

  void setError( Engine::EngineError error, const QString &errorString ) {
    this->error = error;
    this->errorString = errorString;
    qWarning() << "QJSTemplateEngine::Engine:" << errorString;
  }  
  
  QJSValue createDataProperty( QJSEngine *engine ) const {
    QJSValue result = engine->newObject();
    for ( auto propertyName : dataProviders.keys() ) {
      result.setProperty( propertyName, dataProviders.value( propertyName )->data( engine ) );
    }
    return result;
  }
  
  QJSValue createFilesProperty( QJSEngine *engine ) const {
    QJSValue result = engine->newObject();
    for ( auto fileName : files.keys() ) {
      result.setProperty( fileName, files.value( fileName )->data( engine ) );
    }
    return result;
  }
  
};

}

#endif // QJSTEMPLATEENGINE_ENGINE_P_H
