#ifndef QJSTEMPLATEENGINE_CONFIGURATION_P_H
#define QJSTEMPLATEENGINE_CONFIGURATION_P_H

#include "QJSTemplateEngine/configuration.hpp"

#include <QHash>
#include <QStringList>

namespace QJSTemplateEngine {

struct ConfigurationPrivate {
  int                                       maxParserIncludeNestingLevel;
  QStringList                               parserIncludePaths;
  QStringList                               engineIncludePaths;
  Configuration::ParserConfigurationOptions parserOptions;
  Configuration::EngineConfigurationOptions engineOptions;
  QString                                   apiName;
  QHash<QString, const FileLoader* >        fileLoaders;
  
  
  ConfigurationPrivate() :
  maxParserIncludeNestingLevel( -1 ),
  parserIncludePaths(),
  engineIncludePaths(),
  parserOptions( Configuration::NoParserOption ),
  engineOptions( Configuration::NoEngineOption ),
  apiName( "QJSTemplateEngine" ),
  fileLoaders() {
  }
  
};

}
#endif // QJSTEMPLATEENGINE_CONFIGURATION_P_H
