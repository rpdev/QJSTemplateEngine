#include "QJSTemplateEngine/engine.hpp"

#include "QJSTemplateEngine/DataProvider"
#include "QJSTemplateEngine/FileLoader"
#include "QJSTemplateEngine/Generator"
#include "QJSTemplateEngine/Parser"
#include "QJSTemplateEngine/QJSTemplateEngineAPI"

#include "engine_p.hpp"

#include <QBuffer>
#include <QFile>
#include <QFileInfo>
#include <QJSEngine>

namespace QJSTemplateEngine {

/**
   @brief Constructor
 */
Engine::Engine(QObject *parent) : QObject(parent),
  d_ptr( new EnginePrivate() )
{
  
}

/**
   @brief Destructor
 */
Engine::~Engine()
{
  
}

/**
   @brief Returns the configuration used by the engine
 */
SharedConfiguration Engine::configuration() const
{
  const Q_D( Engine );
  return d->configuration;
}

/**
   @brief Sets the configuration to be used by the engine
 */
void Engine::setConfiguration(const SharedConfiguration &configuration)
{
  Q_D( Engine );
  d->configuration = configuration;
}

/**
   @brief Returns the last error encountered by the engine
 */
Engine::EngineError Engine::error() const
{
  const Q_D( Engine );
  return d->error;
}

/**
   @brief Returns the error string of the last error encountered by the engine
 */
QString Engine::errorString() const
{
  const Q_D( Engine );
  return d->errorString;
}

/**
   @brief Renders a template document
   
   This method takes a template @p document and runs it, writing any generated text to the
   given @p output device. Returns true in case rendering succeeded or false if any errors occurred.
 */
bool Engine::render(const AST::Document *document, QIODevice *output)
{
  Q_D( Engine );
  
  Q_ASSERT( document != nullptr );
  
  Generator generator;
  generator.setConfiguration( d->configuration );
  QByteArray js = generator.generate( document );
  return renderJavaScript( js, output );
}

/**
   @brief Renders a template document
   
   This is an overloaded method. It takes a template @p document and writes the generated
   output into the given @p output byte array.
 */
bool Engine::render(const AST::Document *document, QByteArray &output)
{
  Q_D( Engine );
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer:" + buffer.errorString() );
    return false;
  }
  return render( document, &buffer );
}

/**
   @brief Renders a template
   
   This is an overloaded method. It reads a template from the @p input device and
   parses it. Then, the template is run and any generated text written to the @p output device.
 */
bool Engine::render(QIODevice *input, QIODevice *output)
{
  Q_D( Engine );
  
  Q_ASSERT( input != nullptr );
  
  Parser parser;
  parser.setConfiguration( d->configuration );
  if ( !parser.parse( input ) ) {
    d->setError( EngineParserError, parser.errorString() );
    return false;
  }
  
  return render( parser.document(), output );
}

/**
   @brief Renders a template
   
   This is an overloaded method. It reads a template from the @p input device and
   parses it. Then, the template is run and any generated text written to the @p output byte array.
 */
bool Engine::render(QIODevice *input, QByteArray &output)
{
  Q_D( Engine );
  
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return render( input, &buffer );
}

/**
   @brief Renders a template
   
   This is an overloaded method. It takes a template via the @p input byte aray and
   parses it. Then, the template is run and any generated text written to the @p output device.
 */
bool Engine::render(const QByteArray &input, QIODevice *output)
{
  Q_D( Engine );
  
  QBuffer buffer;
  buffer.setData( input );
  if ( !buffer.open( QIODevice::ReadOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return render( &buffer, output );
}

/**
   @brief Renders a template
   
   This is an overloaded method. It takes a template via the @p input byte aray and
   parses it. Then, the template is run and any generated text written to the @p output byte array.
 */
bool Engine::render(const QByteArray &input, QByteArray &output)
{
  Q_D( Engine );

  QBuffer buffer;
  buffer.setData( input );
  if ( !buffer.open( QIODevice::ReadOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return render( &buffer, output );
}

/**
   @brief Renders a template from a file
   
   This method reads a template from the given @p filename, parses it and runs it. Any generated
   text is written to the @p output device. In case everything succeeds, true is returned. If any
   error occurs, false is returned.
 */
bool Engine::renderFile(const QString &filename, QIODevice *output)
{
  Q_D( Engine );
  
  QFile file( filename );
  if ( !file.open( QIODevice::ReadOnly ) ) {
    d->setError( EngineIOError, "Failed to open QFile: " + file.errorString() );
    return false;
  }
  return render( &file, output );
}

/**
   @brief Renders a template from a file
   
   This method reads a template from the given @p filename, parses it and runs it. Any generated
   text is written to the @p output byte array. In case everything succeeds, true is returned. 
   If any error occurs, false is returned.
 */
bool Engine::renderFile(const QString &filename, QByteArray &output)
{
  Q_D( Engine );
  
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return renderFile( filename, &buffer );
}

/**
   @brief Render pre-compiled JavaScript
   
   This method reads a pre-compiled JavaScript from the @p input device and runs it. Any generated
   text is written to the @p output device. In case any errors occur, the method returns false,
   otherwise true.
 */
bool Engine::renderJavaScript(QIODevice *input, QIODevice *output)
{
  Q_D( Engine );
  
  Q_ASSERT( input != nullptr );
  Q_ASSERT( output != nullptr );
  
  QString program = input->readAll();
  QString fileName = "QJSTemplate";
  QFile *fileDevice = qobject_cast<QFile*>( input );
  if ( fileDevice ) {
    fileName = fileDevice->fileName();
  }
  QJSEngine engine;
  QJSTemplateEngineAPI *api = new QJSTemplateEngineAPI();
  api->setEngine( this );
  api->setJavaScriptEngine( &engine );
  api->setOutputDevice( output );
  api->setData( d->createDataProperty( &engine ) );
  api->setFiles( d->createFilesProperty( &engine ) );
  QJSValue jsApi = engine.newQObject( api ); // Note: Ownership goes over to JSEngine
  engine.globalObject().setProperty( d->configuration->apiName(), jsApi );
  
  QJSValue result = engine.evaluate( program, fileName );
  if ( result.isError() ) {
    d->setError( EngineJavaScriptError, "Execution of template failed: " + 
                 result.toString() );
    return false;
  }
  
  if ( api->error() != QJSTemplateEngineAPI::NoError ) {
    d->setError( EngineJavaScriptError, "There was an error during script execution: " +
                 api->errorString() );
    return false;
  }
  
  // Execute blocks:
  api->renderBlocks();
  
  return true;
}

/**
   @brief Render pre-compiled JavaScript
   
   This is an overloaded method. It reads the input from the @p input device and writes any
   generated text to the @p output byte array.
 */
bool Engine::renderJavaScript(QIODevice *input, QByteArray &output)
{
  Q_D( Engine );
  
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return renderJavaScript( input, &buffer );
}

/**
   @brief Render pre-compiled JavaScript
   
   This is an overloaded method. It reads the input from the @p input byte array and writes any
   generated text to the @p output device.
 */
bool Engine::renderJavaScript(const QByteArray &input, QIODevice *output)
{
  Q_D( Engine );
  
  QBuffer buffer;
  buffer.setData( input );
  if ( !buffer.open( QIODevice::ReadOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return renderJavaScript( &buffer, output );
}

/**
   @brief Render pre-compiled JavaScript
   
   This is an overloaded method. It reads the input from the @p input byte array and writes any
   generated text to the @p output byte array.
 */
bool Engine::renderJavaScript(const QByteArray &input, QByteArray &output)
{
  Q_D( Engine );
  
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return renderJavaScript( input, &buffer );
}

/**
   @brief Renders a pre-compiled JavaScript file
   
   This method reads a pre-compiled JavaScript from the given @p filename, executes it and
   writes any generated text into the @p output device.
 */
bool Engine::renderJavaScriptFile(const QString &filename, QIODevice *output)
{
  Q_D( Engine );
  
  QFile file( filename );
  if ( !file.open( QIODevice::ReadOnly ) ) {
    d->setError( EngineIOError, "Failed to open QFile: " + file.errorString() );
    return false;
  }
  
  return renderJavaScript( &file, output );
}

/**
   @brief Renders a pre-compiled JavaScript file
   
   This is an overloaded method. It reads and executes the pre-compiled @p filename and
   writes generated text to the @p output byte array.
 */
bool Engine::renderJavaScriptFile(const QString &filename, QByteArray &output)
{
  Q_D( Engine );
  
  QBuffer buffer( &output );
  if ( !buffer.open( QIODevice::WriteOnly ) ) {
    d->setError( EngineIOError, "Failed to open QBuffer: " + buffer.errorString() );
    return false;
  }
  
  return renderJavaScriptFile( filename, &buffer );
}

/**
   @brief Adds a data provider to the engine
   
   This method adds the given @p dataProvider to the engine. The provider can be accessed using the
   specified @p name. Consider the following C++ code:
   
   @code
   Engine e;
   QByteArray result;
   DataProvider *dp = createCustomDataProvider();
   dp.setParent( &engine );
   e.addDataProvider( "MyData", dp );
   e.renderFile( getTemplatesDir() + "/say_hello.qjst", result );
   @endcode
   
   In the template, the data can be accessed via QJSTemplateEngine.data and the specified name:
   
   @code
   var myData = QJSTemplateEngine.data.MyData;
   @endcode
 */
void Engine::addDataProvider(const QString &name, DataProvider *dataProvider)
{
  Q_D( Engine );
  Q_ASSERT( dataProvider != nullptr );
  d->dataProviders[name] = dataProvider;
}

/**
   @brief Loads a file and makes it available to the template
   
   This method will load the file specified by the given @p fileName, making
   it available to the Template via
   
   @code
   var fileData = QJSTemplateEngine.files[fileName];
   @endcode
   
   The concrete data structure that the template will see depends on the
   type of DataProvider created by the underlying FileLoader used for loading.
   In order to select a FileLoader, this method will use the file name suffix of
   the file. In order to allow specific file loader to be implemented (e.g. for a specific
   XML dialect), the method first checks if a data loader for the complete suffix
   is available (see also QFileInfo::completeSuffix()). If no loader for the complete
   suffix is found, then only the minimal suffix is tried
   (see also QFileInfo::suffix()).
   
   If loading the file succeeds, the method returns true. Otherwise, false is returned.
 */
bool Engine::addFileBySuffix(const QString &fileName)
{
  Q_D( Engine );
  QFileInfo fi( fileName );
  // First try the complete file name suffix (allows for a most-specific first approach):
  QString completeSuffix = fi.completeSuffix();
  const FileLoader *loader = d->configuration->fileLoaderByFileNameSuffix( fi.completeSuffix() );
  if ( !loader ) {
    QString suffix = fi.suffix();
    if ( completeSuffix != suffix ) {
      loader = d->configuration->fileLoaderByFileNameSuffix( fi.suffix() );
    }
  }
  if ( loader ) {
    DataProvider *data = loader->loadFile( fileName );
    if ( data ) {
      data->setParent( this );
      if ( d->files.contains( fileName ) ) {
        delete d->files.value( fileName );
      }
      d->files.insert( fileName, data );
      return true;
    }
  }
  return false;
}

/**
   @brief Loads a file and makes it available to the template
   
   This method is similar to the addFile() method, except it uses the
   MIME type of the referenced file to decide which of the FileLoader objects
   registered in the Configuration of the engine to use.
 */
bool Engine::addFile(const QString &fileName)
{
  Q_D( Engine );
  const FileLoader *loader = d->configuration->fileLoaderByMimeType( fileName );
  if ( loader ) {
    DataProvider *data = loader->loadFile( fileName );
    if ( data ) {
      data->setParent( this );
      if ( d->files.contains( fileName ) ) {
        delete d->files.value( fileName );
      }
      d->files.insert( fileName, data );
      return true;
    }
  }
  return false;
}

} // namespace QJSTemplateEngine

