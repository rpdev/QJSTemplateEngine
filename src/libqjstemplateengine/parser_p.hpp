#ifndef QJSTEMPLATEENGINE_PARSER_P_H
#define QJSTEMPLATEENGINE_PARSER_P_H

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Parser"
#include "QJSTemplateEngine/Configuration"

#include <QRegExp>
#include <QDebug>
#include <QDir>
#include <QFile>

namespace QJSTemplateEngine {

struct ParserPrivate {
  
  enum ParserState {
    TextState,
    FoundOpeningAngularState,
    ExecutableJavaScriptState,
    FoundClosingQuestionMarkState,
    FoundOpeningDollarState,
    PrintableJavaScriptState
  };

  AST::Document *ast;
  Parser::ParserError error;
  QString errorString;
  SharedConfiguration configuration;
  
  ParserPrivate() :
    ast( nullptr ),
    error( Parser::NoError ),
    errorString(),
    configuration( new Configuration() ) {
  }
  
  void setParserError( Parser::ParserError error, const QString &errorString ) {
    this->error = error;
    this->errorString = errorString;
    qWarning() << "QJSTemplateEngine::Parser:" << errorString;
  }
  
  void clear() {
    if ( ast ) {
      delete ast;
      ast = nullptr;
    }
    error = Parser::NoError;
    errorString.clear();
  }
  
  bool readChar( char &c, QIODevice *device ) {
    if ( !device->getChar( &c ) ) {
      setParserError( Parser::ParserIOError, "Failed to get char from device: " +
                      device->errorString() );
      return false;
    }
    return true;
  }
  
  bool checkNewNode( AST::Node *newNode ) {
    if ( newNode == nullptr ) {
      setParserError( Parser::ParserOUtOfMemoryError,
                      "Unable to allocate new AST node - probably out of memory" );
      return false;
    }
    return true;
  }
  
  bool extractIncludeFileName( QByteArray &fileName, const QByteArray &buffer ) {
    QString str = buffer;
    QRegExp re( "^include\\s*\"([^\"]*)\"\\s*$");
    if ( re.indexIn( str ) == 0 ) {
      fileName = re.cap( 1 ).toUtf8();
      return true;
    }
    return false;
  }
  
  template<typename T>
  bool parse( QIODevice *device, T* parentNode ) {
    if ( !device->isOpen() ) {
      setParserError( Parser::ParserIOError, "The device is not open!" );
      return false;
    }
    
    ParserState currentState = TextState;
    QByteArray buffer;

    char nextChar;
    while ( device->getChar( &nextChar ) ) {
      switch ( currentState ) {
      
      case TextState:
        switch ( nextChar ) {
        case '<':
        {
          currentState = FoundOpeningAngularState;
          break;
        }
        case '$':
        {
          currentState = FoundOpeningDollarState;
          break;
        }
        default: break;
        }
        buffer.append( nextChar );
        break;
        
      case FoundOpeningAngularState:
        switch ( nextChar ) {
        case '?':
        {
          buffer = buffer.left( buffer.length() - 1 ); // remove opening angular
          if ( !buffer.isEmpty() ) {
            AST::TextNode *textNode = parentNode->createTextNode();
            if ( !checkNewNode( textNode ) ) {
              return false;
            }
            textNode->setText( buffer );
          }
          currentState = ExecutableJavaScriptState;
          buffer.clear();
          break;
        }
        default:
        {
          buffer.append( nextChar );
          currentState = TextState;
          break;
        }
        }
        break;
        
      case FoundOpeningDollarState:
        switch ( nextChar ) {
        case '{':
        {
          buffer = buffer.left( buffer.length() - 1 ); // remove dollar
          if ( !buffer.isEmpty() ) {
            AST::TextNode *textNode = parentNode->createTextNode();
            if ( !checkNewNode( textNode ) ) {
              return false;
            }
            textNode->setText( buffer );
          }
          currentState = PrintableJavaScriptState;
          buffer.clear();
          break;
        }
        default:
        {
          buffer.append( nextChar );
          currentState = TextState;
          break;
        }
        }
        break;
        
      case ExecutableJavaScriptState:
        switch ( nextChar ) {
        case '?':
        {
          currentState = FoundClosingQuestionMarkState;
          break;
        }
        default: break;
        }
        buffer.append( nextChar );
        break;
        
      case FoundClosingQuestionMarkState:
        switch ( nextChar ) {
        case '>':
        {
          buffer = buffer.left(buffer.length() - 1 ); // remove question mark
          QByteArray fileName;
          if ( extractIncludeFileName( fileName, buffer ) ) {
            AST::IncludeNode *includeNode = parentNode->createIncludeNode();
            if ( !checkNewNode( includeNode ) ) {
              return false;
            }
            includeNode->setFileName( fileName );
          } else {
            AST::JavaScriptNode *jsNode = parentNode->createJavaScriptNode();
            if ( !checkNewNode( jsNode ) ) {      
              return false;
            }
            jsNode->setJavaScript( buffer );
            jsNode->setPrint( false );
          }
          buffer.clear();
          currentState = TextState;
          break;
        }
        default:
        {
          buffer.append( nextChar );
          currentState = ExecutableJavaScriptState;
          break;
        }
        }
        break;
        
      case PrintableJavaScriptState:
        switch ( nextChar ) {
        case '}':
        {
          AST::JavaScriptNode *jsNode = parentNode->createJavaScriptNode();
          if ( !checkNewNode( jsNode ) ) {
            return false;
          }
          jsNode->setJavaScript( buffer );
          jsNode->setPrint( true );
          buffer.clear();
          currentState = TextState;
          break;
        }
        default:
        {
          buffer.append( nextChar );
          break;
        }
        }
        break;
        
      }
    }
    
    if ( !buffer.isEmpty() ) {
      switch ( currentState ) {
      case TextState: // fall through
      case FoundOpeningAngularState: // fall through
      case FoundOpeningDollarState: 
      {
        AST::TextNode *textNode = parentNode->createTextNode();
        if ( !checkNewNode( textNode ) ) {
          return false;
        }
        textNode->setText( buffer );
        break;
      }
        
      case ExecutableJavaScriptState:
      case FoundClosingQuestionMarkState:
      {
        QByteArray fileName;
        if ( extractIncludeFileName( fileName, buffer ) ) {
          AST::IncludeNode *includeNode = parentNode->createIncludeNode();
          if ( !checkNewNode( includeNode ) ) {
            return false;
          }
          includeNode->setFileName( fileName );
        } else {
          AST::JavaScriptNode *jsNode = parentNode->createJavaScriptNode();
          if ( !checkNewNode( jsNode ) ) {
            return false;
          }
          jsNode->setJavaScript( buffer );
          jsNode->setPrint( false );
        }
        break;
      }
        
      case PrintableJavaScriptState:
      {
        AST::JavaScriptNode *jsNode = parentNode->createJavaScriptNode();
        if ( !checkNewNode( jsNode ) ) {
          return false;
        }
        jsNode->setJavaScript( buffer );
        jsNode->setPrint( true );
        break;
      }
      }
    }
    
    return true;
  }
  
  bool parseRecursively( AST::IncludeNode *includeNode, int level ) {
    if ( !configuration->parserOptions().testFlag( Configuration::EnableParserIncludes ) ) {
      setParserError( Parser::ParserIncludesDisabledError, 
                      "Found include statement but parser includes are disabled." );
      return false;
    }
    int maxNestingLevel = configuration->maxParserIncludeNestingLevel();
    if ( maxNestingLevel >= 0 && level < maxNestingLevel ) {
      setParserError( Parser::ParserMaxNestingLevelExceededError,
                      QString( "The maximum nesting level of %1 has been exceeded by include of %2" )
                      .arg( maxNestingLevel ).arg( QString( includeNode->fileName() ) ) );
      return false;
    }
    for ( auto searchPath : configuration->parserIncludePaths() ) {
      QDir dir( searchPath );
      QString absoluteFilePath = dir.absoluteFilePath( includeNode->fileName() );
      QFile file( absoluteFilePath );
      if ( file.exists() ) {
        if ( file.open( QIODevice::ReadOnly ) ) {
          if ( !parse( &file, includeNode ) ) {
            return false;
          }
          for ( AST::IncludeNode *include : includeNode->childNodesByType<AST::IncludeNode>() ) {
            if ( !parseRecursively( include, level * 1 ) ) {
              return false;
            }
          }
          return true;
        }
      }
    }
    setParserError( Parser::ParserIncludeFileNotFoundError,
                    QString( "The referenced parser include file %1 could not be found" )
                    .arg( QString( includeNode->fileName() ) ) );
    return false;
  }
  
};

}
#endif // QJSTEMPLATEENGINE_PARSER_P_H
