#include "QJSTemplateEngine/plaintextfileloader.hpp"

#include "plaintextfileloader_p.hpp"

#include "QJSTemplateEngine/QVariantDataProvider"

#include <QDebug>
#include <QFile>
#include <QMimeDatabase>
#include <QTextStream>

namespace QJSTemplateEngine {

/**
   @brief Name name (see also FileLoader::name()) that is used by the PlainTextFileLoader
 */
const QString PlainTextFileLoader::Name = "PlainText";

PlainTextFileLoader::PlainTextFileLoader(QObject *parent) : 
  FileLoader( new PlainTextFileLoaderPrivate(), 
              Name, { "txt" }, { QMimeDatabase().mimeTypeForName( "text/plain" ) }, 
              parent )
{
}

DataProvider *PlainTextFileLoader::loadFile(const QString &fileName) const
{
  QFile file( fileName );
  QStringList lines;
  if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
    QTextStream stream( &file );
    while ( !stream.atEnd() ) {
      lines << stream.readLine();
    }
    file.close();
    return new QVariantDataProvider( lines );
  }
  qWarning() << "Failed to open plain text file:" << file.errorString();
  return nullptr;
}

} // namespace QJSTemplateEngine

