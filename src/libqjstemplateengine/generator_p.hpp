#ifndef QJSTEMPLATEENGINE_GENERATOR_P_HPP
#define QJSTEMPLATEENGINE_GENERATOR_P_HPP

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Configuration"

#include <QTextStream>

namespace QJSTemplateEngine {

struct GeneratorPrivate {
  
  SharedConfiguration configuration;
  
  GeneratorPrivate() :
    configuration( new Configuration() ) {
  }
  
  QString escapeJSString( const QByteArray &str ) const {
    QByteArray copy = str;
    return copy.replace( "\\", "\\\\" )
        .replace( "\n", "\\n" )
        .replace( "\r", "\\r" )
        .replace( "\"", "\\\"" );
  }
  
  QString printString( const QByteArray &str ) const {
    return QString("%1.print(\"%2\");\n" )
        .arg( configuration->apiName() )
        .arg( escapeJSString( str ) );
  }
  
  QString printJS( const QString &expr ) const {
    return QString( "%1.print(%2);\n" )
        .arg( configuration->apiName() )
        .arg( expr );
  }
  
  template<typename NodeType>
  void generate( NodeType *node, QTextStream &output ) const {
    for ( AST::Node *child : node->childNodes() ) {
      if ( AST::TextNode *text = dynamic_cast< AST::TextNode* >( child ) ) {
        output << printString( text->text() );
      } else if ( AST::JavaScriptNode *js = dynamic_cast< AST::JavaScriptNode* >( child ) ) {
        if ( js->print() ) {
          output << printJS( js->javaScript() );
        } else {
          output << js->javaScript() << "\n";
        }
      } else if ( AST::IncludeNode *include = dynamic_cast<AST::IncludeNode*>( child ) ) {
        generate( include, output );
      }
    }
  }
  
};

}

#endif // QJSTEMPLATEENGINE_GENERATOR_P_HPP

