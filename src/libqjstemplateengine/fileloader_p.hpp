#ifndef QJSTEMPLATEENGINE_FILELOADER_P_HPP
#define QJSTEMPLATEENGINE_FILELOADER_P_HPP

#include <QList>
#include <QMimeType>
#include <QString>
#include <QStringList>

namespace QJSTemplateEngine {

struct FileLoaderPrivate {
  QString           name;
  QStringList       supportedFileNameExtensions;
  QList<QMimeType>  supportedMimeTypes;
};

}
#endif // QJSTEMPLATEENGINE_FILELOADER_P_HPP

