#include "QJSTemplateEngine/fileloader.hpp"

#include "fileloader_p.hpp"

namespace QJSTemplateEngine {

FileLoader::FileLoader(const QString &name, 
                       const QStringList &supportedFileNameExtensions, 
                       const QList<QMimeType> &supportedMimeTypes, 
                       QObject *parent) : 
  QObject(parent),
  d_ptr( new FileLoaderPrivate() )
{
  Q_D( FileLoader );
  d->name = name;
  d->supportedFileNameExtensions = supportedFileNameExtensions;
  d->supportedMimeTypes = supportedMimeTypes;
}

FileLoader::~FileLoader()
{
  
}

/**
   @brief The name of the loader
   
   This is used in order to identify loader uniquely. Each loader registered in
   a Configuration must have a unique name.
 */
QString FileLoader::name() const
{
  const Q_D( FileLoader );
  return d->name;
}

/**
   @brief The list of explicitly supported file name extensions of a loader
   
   This returns the list of file name extensions that the loader can handle.
   This can be used in order to implement loaders for very specific files
   (where no dedicated MIME type exists). In this case, an identification
   by (custom) file name extension can be implemented.
 */
QStringList FileLoader::supportedFileNameExtensions() const
{
  const Q_D( FileLoader );
  return d->supportedFileNameExtensions;
}

/**
   @brief The list of supported MIME types of that loader
   
   This returns a list of MIME types that this loader supports. Might be empty
   (in this case, the loader either must be requested via its name or
   by using special file name extensions).
   
   @sa supportedFileNameExtensions()
 */
QList<QMimeType> FileLoader::suportedMimeTypes() const
{
  const Q_D( FileLoader );
  return d->supportedMimeTypes;
}

FileLoader::FileLoader( FileLoaderPrivate *d, 
                        const QString &name, 
                        const QStringList &supportedFileNameExtensions, 
                        const QList<QMimeType> &supportedMimeTypes, 
                        QObject *parent ) :
  QObject( parent ),
  d_ptr( d )
{
  d_ptr->name = name;
  d_ptr->supportedFileNameExtensions = supportedFileNameExtensions;
  d_ptr->supportedMimeTypes = supportedMimeTypes;
}

} // namespace QJSTemplateEngine

