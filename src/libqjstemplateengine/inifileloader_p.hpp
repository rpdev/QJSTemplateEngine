#ifndef INIFILEFORMAT_P_HPP
#define INIFILEFORMAT_P_HPP

#include "fileloader_p.hpp"

#include <QSettings>

namespace QJSTemplateEngine {

class INIFileLoaderPrivate : public FileLoaderPrivate
{
public:
  
  /**
     @brief Converts a QSettings object to QVariant
     
     This converts a QSettings object into a QVariant. It constructs
     a QVariantMap with the direct child keys directly added as well as
     groups being transformed to sub-maps (which in turn recursively
     contain converted data).
   */
  QVariant settingsToVariant( QSettings &settings ) const {
    QVariantMap result;
    for ( auto key : settings.childKeys() ) {
      result.insert( key, settings.value( key ) );
    }
    for ( auto group : settings.childGroups() ) {
      settings.beginGroup( group );
      result.insert( group, settingsToVariant( settings ) );
      settings.endGroup();
    }
    return result;
  }
};

}
#endif // INIFILEFORMAT_P_HPP

