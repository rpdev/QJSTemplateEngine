include(../../config.pri)
setupLibrary(qjstemplateengine)

QT += qml

INCLUDEPATH += QJSTemplateEngine

HEADERS += \
    QJSTemplateEngine/config.hpp \
    QJSTemplateEngine/configuration.hpp \
    QJSTemplateEngine/parser.hpp \
    QJSTemplateEngine/engine.hpp \
    QJSTemplateEngine/Configuration \
    QJSTemplateEngine/Engine \
    QJSTemplateEngine/Parser \
    QJSTemplateEngine/QJSTemplateEngine \
    configuration_p.hpp \
    engine_p.hpp \
    parser_p.hpp \
    QJSTemplateEngine/ast.hpp \
    QJSTemplateEngine/AST \
    ast_p.hpp \
    QJSTemplateEngine/generator.hpp \
    QJSTemplateEngine/Generator \
    generator_p.hpp \
    QJSTemplateEngine/qjstemplateengineapi.hpp \
    QJSTemplateEngine/QJSTemplateEngineAPI \
    qjstemplateengineapi_p.hpp \
    QJSTemplateEngine/dataprovider.hpp \
    dataprovider_p.hpp \
    QJSTemplateEngine/DataProvider \
    QJSTemplateEngine/qvariantdataprovider.hpp \
    QJSTemplateEngine/QVariantDataProvider \
    qvariantdataprovider_p.hpp \
    QJSTemplateEngine/fileloader.hpp \
    QJSTemplateEngine/FileLoader \
    fileloader_p.hpp \
    QJSTemplateEngine/plaintextfileloader.hpp \
    QJSTemplateEngine/PlainTextFileLoader \
    plaintextfileloader_p.hpp \
    QJSTemplateEngine/jsonfileloader.hpp \
    jsonfileloader_p.hpp \
    inifileloader_p.hpp \
    QJSTemplateEngine/inifileloader.hpp \
    QJSTemplateEngine/INIFileLoader

SOURCES += \
    configuration.cpp \
    parser.cpp \
    engine.cpp \
    ast.cpp \
    generator.cpp \
    qjstemplateengineapi.cpp \
    dataprovider.cpp \
    qvariantdataprovider.cpp \
    fileloader.cpp \
    plaintextfileloader.cpp \
    plaintextfileloader_p.cpp \
    jsonfileloader.cpp \
    inifileformat.cpp

headers.files = QJSTemplateEngine/*
headers.path = $$HEADER_PATH/QJSTemplateEngine
INSTALLS += target
