#include "QJSTemplateEngine/ast.hpp"

#include "ast_p.hpp"

namespace QJSTemplateEngine {

namespace AST {

Node::Node(QObject *parent) :
  QObject( parent ),
  d_ptr( new NodePrivate() )
{
}

Node::~Node()
{
}

Node::Node(NodePrivate *d, QObject *parent) :
  QObject( parent ),
  d_ptr( d )
{
}

Document::Document(QObject *parent) :
  Node( new DocumentPrivate(), parent )
{
}

Document::~Document()
{
}

QList<Node *> Document::childNodes() const
{
  const Q_D( Document );
  return d->nodes;
}

TextNode *Document::createTextNode()
{
  Q_D( Document );
  TextNode* result = new TextNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

JavaScriptNode *Document::createJavaScriptNode()
{
  Q_D( Document );
  JavaScriptNode *result = new JavaScriptNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

IncludeNode *Document::createIncludeNode()
{
  Q_D( Document );
  IncludeNode *result = new IncludeNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

TextNode::TextNode(Node *parent) :
  Node( new TextNodePrivate(), parent )
{
  
}

TextNode::~TextNode()
{
}

QByteArray TextNode::text() const
{
  const Q_D( TextNode );
  return d->text;
}

void TextNode::setText(const QByteArray &text)
{
  Q_D( TextNode );
  d->text = text;
}

JavaScriptNode::JavaScriptNode(Node *parent) :
  Node( new JavaScriptNodePrivate(), parent )
{
}

JavaScriptNode::~JavaScriptNode()
{
}

QByteArray JavaScriptNode::javaScript() const
{
  const Q_D( JavaScriptNode );
  return d->javaScript;
}

void JavaScriptNode::setJavaScript(const QByteArray &javaScript)
{
  Q_D( JavaScriptNode );
  d->javaScript = javaScript;
}

bool JavaScriptNode::print() const
{
  const Q_D( JavaScriptNode );
  return d->print;
}

void JavaScriptNode::setPrint(bool print)
{
  Q_D( JavaScriptNode );
  d->print = print;
}

IncludeNode::IncludeNode(Node *parent) :
  Node( new IncludeNodePrivate(), parent )
{
}

IncludeNode::~IncludeNode()
{
}

QByteArray IncludeNode::fileName() const
{
  const Q_D( IncludeNode );
  return d->fileName;
}

void IncludeNode::setFileName(const QByteArray &fileName)
{
  Q_D( IncludeNode );
  d->fileName = fileName;
}

QList<Node *> IncludeNode::childNodes() const
{
  const Q_D( IncludeNode );
  return d->nodes;
}

TextNode *IncludeNode::createTextNode()
{
  Q_D( IncludeNode );
  TextNode *result = new TextNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

JavaScriptNode *IncludeNode::createJavaScriptNode()
{
  Q_D( IncludeNode );
  JavaScriptNode *result = new JavaScriptNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

IncludeNode *IncludeNode::createIncludeNode()
{
  Q_D( IncludeNode );
  IncludeNode *result = new IncludeNode( this );
  if ( result ) {
    d->nodes.append( result );
  }
  return result;
}

} // namespace AST

} // namespace QJSTemplateEngine

