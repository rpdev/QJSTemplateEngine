#include "QJSTemplateEngine/jsonfileloader.hpp"

#include "jsonfileloader_p.hpp"

#include "QJSTemplateEngine/QVariantDataProvider"

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QMimeDatabase>

namespace QJSTemplateEngine {

const QString JSONFileLoader::Name = "JSON";

JSONFileLoader::JSONFileLoader(QObject *parent) : 
  FileLoader( new JSONFileLoaderPrivate(), 
              Name, { "json" }, 
             { QMimeDatabase().mimeTypeForName("application/json") }, parent )
{
}

DataProvider *JSONFileLoader::loadFile(const QString &fileName) const
{
  QFile file( fileName );
  if ( !file.open( QIODevice::ReadOnly ) ) {
    qWarning() << file.errorString();
    return nullptr;
  }
  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson( file.readAll(), &error );
  if ( error.error != QJsonParseError::NoError ) {
    qWarning() << "Failed to parse" << fileName << " - " << error.errorString();
    return nullptr;
  }
  return new QVariantDataProvider( doc.toVariant() );
}

} // namespace QJSTemplateEngine

