#include "QJSTemplateEngine/dataprovider.hpp"

#include "dataprovider_p.hpp"

namespace QJSTemplateEngine {

/**
   @brief Constructor
 */
DataProvider::DataProvider(QObject *parent) : QObject(parent),
  d_ptr( new DataProviderPrivate() )
{
  
}

/**
   @brief Destructor
 */
DataProvider::~DataProvider()
{
  
}

/**
   @brief Protected constructor to re-use the d-pointer
   
   This constructor can be used by sub-classes of the DataProvider class if they want to reuse the
   d-pointer. For this, the sub-class can pass in its customized d-pointer (which must
   derive from DataProviderPrivate).
 */
DataProvider::DataProvider(DataProviderPrivate *d, QObject *parent) :
  QObject( parent ),
  d_ptr( d )
{
}

} // namespace QJSTemplateEngine

