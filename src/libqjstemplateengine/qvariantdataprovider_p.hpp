#ifndef QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_P_HPP
#define QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_P_HPP

#include "dataprovider_p.hpp"

#include <QVariant>

namespace QJSTemplateEngine {

struct QVariantDataProviderPrivate : DataProviderPrivate {
  
  QVariant data;
  
};

}
#endif // QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_P_HPP

