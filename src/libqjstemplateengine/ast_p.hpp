#ifndef QJSTEMPLATEENGINE_AST_P_HPP
#define QJSTEMPLATEENGINE_AST_P_HPP

#include "QJSTemplateEngine/ast.hpp"

#include <QByteArray>
#include <QList>

namespace QJSTemplateEngine {

namespace AST {

struct NodePrivate {
};

struct DocumentPrivate : NodePrivate {
  QList<Node*> nodes;
  
  ~DocumentPrivate() {
    for ( auto node : nodes ) {
      delete node;
    }
  }
};

struct TextNodePrivate : NodePrivate {
  QByteArray text;
  
  TextNodePrivate() :
    text() {
  }
  
};

struct JavaScriptNodePrivate : NodePrivate {
  QByteArray  javaScript;
  bool        print;
  
  JavaScriptNodePrivate() :
    javaScript(),
    print( false ) {
  }
  
};

struct IncludeNodePrivate : NodePrivate {
  QByteArray fileName;
  QList<Node*> nodes;
  
  IncludeNodePrivate() :
    fileName(),
    nodes() {
  }

  ~IncludeNodePrivate() {
    for ( auto node : nodes ) {
      delete node;
    }
  }
};

}

}
#endif // QJSTEMPLATEENGINE_AST_P_HPP

