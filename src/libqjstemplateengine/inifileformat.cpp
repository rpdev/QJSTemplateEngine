#include "QJSTemplateEngine/inifileloader.hpp"

#include "inifileloader_p.hpp"

#include "QJSTemplateEngine/QVariantDataProvider"

#include <QSettings>

namespace QJSTemplateEngine {

const QString INIFileLoader::Name = "INI";

INIFileLoader::INIFileLoader(QObject *parent) : 
  FileLoader( new INIFileLoaderPrivate(),
              Name, 
              { "ini" }, 
              {}, // Note: No MIME support, as INI has type text/plain!
              parent )
{
}

DataProvider *INIFileLoader::loadFile(const QString &fileName) const
{
  const Q_D( INIFileLoader );
  QSettings settings( fileName, QSettings::IniFormat );
  return new QVariantDataProvider( d->settingsToVariant( settings ) );
}

} // namespace QJSTemplateEngine

