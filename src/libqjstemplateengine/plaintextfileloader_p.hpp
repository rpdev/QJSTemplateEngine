#ifndef QJSTEMPLATEENGINE_PLAINTEXTFILELOADERPRIVATE_H
#define QJSTEMPLATEENGINE_PLAINTEXTFILELOADERPRIVATE_H

#include "fileloader_p.hpp"

namespace QJSTemplateEngine {

class PlainTextFileLoaderPrivate : public FileLoaderPrivate
{
public:
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_PLAINTEXTFILELOADERPRIVATE_H
