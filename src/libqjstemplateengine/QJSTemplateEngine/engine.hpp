#ifndef QJSTEMPLATEENGINE_ENGINE_H
#define QJSTEMPLATEENGINE_ENGINE_H

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Configuration"
#include "QJSTemplateEngine/Parser"

#include <QObject>
#include <QIODevice>
#include <QScopedPointer>

namespace QJSTemplateEngine {

// Forward
class EnginePrivate;
class DataProvider;

/**
   @brief The actual QJS template engine
   
   This class implements the actual engine for running (transpiled) JavaScript to generate output.
   For this, it provides several render(), renderFile(), renderJavaScript() and
   renderJavaScriptFile(). The render() and renderFile() methods can be used to transpile and run
   a QJST template. If output has already has been transpiled, then the generated JavaScript can
   be executed via the renderJavaScript() and renderJavaScriptFile() methods.
 */
class Engine : public QObject
{
  Q_OBJECT
  
public:
  
  enum EngineError {
    NoError,
    EngineParserError,
    EngineIOError,
    EngineJavaScriptError
  };

  explicit Engine(QObject *parent = 0);
  ~Engine();

  SharedConfiguration configuration() const;
  void setConfiguration( const SharedConfiguration &configuration );
  
  EngineError error() const;
  QString errorString() const;
  
  bool render( const AST::Document *document, QIODevice *output );
  bool render( const AST::Document *document, QByteArray &output );
  bool render( QIODevice *input, QIODevice *output );
  bool render( QIODevice *input, QByteArray &output );
  bool render( const QByteArray &input, QIODevice *output );
  bool render( const QByteArray &input, QByteArray &output );
  bool renderFile( const QString &filename, QIODevice *output );
  bool renderFile( const QString &filename, QByteArray &output );
  bool renderJavaScript( QIODevice *input, QIODevice *output );
  bool renderJavaScript( QIODevice *input, QByteArray &output );
  bool renderJavaScript( const QByteArray &input, QIODevice *output );
  bool renderJavaScript( const QByteArray &input, QByteArray &output );
  bool renderJavaScriptFile( const QString &filename, QIODevice *output );
  bool renderJavaScriptFile( const QString &filename, QByteArray &output );
  
  void addDataProvider( const QString &name, DataProvider *dataProvider );
  bool addFileBySuffix( const QString &fileName );
  bool addFile( const QString &fileName );
  
  
  
signals:
  
public slots:
  
private:
  
  QScopedPointer<EnginePrivate> d_ptr;
  Q_DECLARE_PRIVATE( Engine )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_ENGINE_H
