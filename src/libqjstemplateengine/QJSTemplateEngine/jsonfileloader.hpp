#ifndef QJSTEMPLATEENGINE_JSONFILELOADER_H
#define QJSTEMPLATEENGINE_JSONFILELOADER_H

#include "QJSTemplateEngine/FileLoader"

#include <QObject>

namespace QJSTemplateEngine {

// forward declaration
class JSONFileLoaderPrivate;

class JSONFileLoader : public FileLoader
{
  Q_OBJECT
public:
  
  static const QString Name;
  
  explicit JSONFileLoader(QObject *parent = 0);
  
  // FileLoader interface
  DataProvider *loadFile(const QString &fileName) const override;
  
signals:
  
public slots:
  
private:
  
  Q_DECLARE_PRIVATE( JSONFileLoader )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_JSONFILELOADER_H
