#ifndef QJSTEMPLATEENGINE_GENERATOR_H
#define QJSTEMPLATEENGINE_GENERATOR_H

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Configuration"

#include <QByteArray>
#include <QObject>
#include <QScopedPointer>

namespace QJSTemplateEngine {

// Forward:
class GeneratorPrivate;

/**
   @brief Transforms documents into JavaScript
   
   This class is used to transform a previously loaded template which has been
   processed via a Parser and transformed into an AST::Document and translates it into
   JavaScript, which can be executed in an Engine.
 */
class Generator : public QObject
{
  Q_OBJECT
  
public:
  explicit Generator(QObject *parent = 0);
  ~Generator();
  
  SharedConfiguration configuration() const;
  void setConfiguration( const SharedConfiguration &configuration );
  
  QByteArray generate( const AST::Document *document );
  
signals:
  
public slots:
  
private:
  QScopedPointer<GeneratorPrivate> d_ptr;
  Q_DECLARE_PRIVATE( Generator )
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_GENERATOR_H
