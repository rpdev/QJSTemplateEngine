#ifndef QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_H
#define QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_H

#include "QJSTemplateEngine/DataProvider"

#include <QObject>

namespace QJSTemplateEngine {

// Forward:
class QVariantDataProviderPrivate;

class QVariantDataProvider : public DataProvider
{
  Q_OBJECT
  
public:
  explicit QVariantDataProvider(const QVariant &data, QObject *parent = 0);
  explicit QVariantDataProvider( QObject *parent = 0 );
  ~QVariantDataProvider();
  
  QVariant wrappedData() const;
  void setWrappedData( const QVariant &data );
  
  // DataProvider interface
  QJSValue data(QJSEngine *scriptEngine) override;
  
signals:
  
public slots:
  
private:
  
  Q_DECLARE_PRIVATE( QVariantDataProvider )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_QVARIANTDATAPROVIDER_H
