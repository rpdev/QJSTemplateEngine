#ifndef QJSTEMPLATEENGINE_CONFIGURATION_H
#define QJSTEMPLATEENGINE_CONFIGURATION_H

#include "config.hpp"

#include <QFileInfo>
#include <QObject>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QStringList>


namespace QJSTemplateEngine {

// Forward declaration
class ConfigurationPrivate;
class FileLoader;

/**
   @brief Encapsulates the configuration of the template system
   
   The Configuration class is used to encapsulate the configuration of various aspects of the
   template engine.
 */
class QJSTEMPLATEENGINE_EXPORT Configuration : public QObject
{
  Q_OBJECT
  
  Q_PROPERTY(int maxParserIncludeNestingLevel READ maxParserIncludeNestingLevel 
             WRITE setMaxParserIncludeNestingLevel NOTIFY maxParserIncludeNestingLevelChanged)
  Q_PROPERTY(QStringList parserIncludePaths READ parserIncludePaths WRITE setParserIncludePaths 
             NOTIFY parserIncludePathsChanged)
  Q_PROPERTY(QStringList engineIncludePaths READ engineIncludePaths WRITE setEngineIncludePaths 
             NOTIFY engineIncludePathsChanged)
  Q_PROPERTY(ParserConfigurationOptions parserOptions READ parserOptions 
             WRITE setParserOptions NOTIFY parserOptionsChanged)
  Q_PROPERTY(EngineConfigurationOptions engineOptions READ engineOptions 
             WRITE setEngineOptions NOTIFY engineOptionsChanged)
  Q_PROPERTY(QString apiName READ apiName WRITE setApiName NOTIFY apiNameChanged)
public:
  
  /**
     @brief Options for configuring the Parser
     
     The values in this enum are used to describe various options for the Parser, i.e. the step
     of the template process which reads in the *.qjst files and transforms them into a runtime
     representation.
     
   */
  enum ParserConfigurationOption {
    /**
      @brief No parser option
      */
    NoParserOption = 0,
    
    /**
      @brief Enables parser includes
      
      If this option is enabled, a template file can include other files (as fragments) via
      
      @code
      <?include "common_file_header.qjst"?>
      @endcode
      
      If parser includes are disabled, input instructions will result in parser
      errors.
      */
    EnableParserIncludes = 0x00000001
  };
  
  Q_DECLARE_FLAGS( ParserConfigurationOptions, ParserConfigurationOption )
  Q_FLAG( ParserConfigurationOptions )
  
  /**
     @brief Options for configuring the Engine
     
     The values in this enum are used to configure aspects of the Engine.
   */
  enum EngineConfigurationOption {
    
    /**
      @brief No engine option
      */
    NoEngineOption = 0,
    
    /**
      @brief Enable JavaScript includes
      
      If this option is enabled, a JavaScript can include another JavaScript libraries via
      
      @code
      QJSTemplateEngine.include( "./common_functions.js" );
      @endcode
      
      If the include succeeds, the method returns true.
      
      If the option is not set, the require() API will always return false.
      */
    EnableEngineIncludes = 0x00000001
  };
  
  Q_DECLARE_FLAGS(EngineConfigurationOptions, EngineConfigurationOption)
  Q_FLAG( EngineConfigurationOptions )

  explicit Configuration(QObject *parent = 0);
  virtual ~Configuration();
  
  int maxParserIncludeNestingLevel() const;
  void setMaxParserIncludeNestingLevel(int maxParserIncludeNestingLevel);
  
  QStringList parserIncludePaths() const;
  void setParserIncludePaths( const QStringList &parserIncludePaths );
  bool addParserIncludePath( const QString &path );
  
  QStringList engineIncludePaths() const;
  void setEngineIncludePaths( const QStringList &engineIncludePaths );
  bool addEngineIncludePath( const QString &path );
  
  ParserConfigurationOptions parserOptions() const;
  void setParserOptions( const ParserConfigurationOptions &parserOptions );
  void enableParserOption( ParserConfigurationOption option );
  void disableParserOption( ParserConfigurationOption option );
  
  EngineConfigurationOptions engineOptions() const;
  void setEngineOptions( const EngineConfigurationOptions &options );
  void enableEngineOption( EngineConfigurationOption option );
  void disableEngineOption( EngineConfigurationOption option );
  
  QString apiName() const;
  void setApiName( const QString &apiName );
  
  void registerFileLoader( FileLoader *fileLoader );
  const FileLoader *fileLoaderByName( const QString &name ) const;
  const FileLoader *fileLoaderByFileNameSuffix( const QString &suffix ) const;
  const FileLoader *fileLoaderByMimeType( const QString &fileName ) const;
  const FileLoader *fileLoaderByMimeType( const QFileInfo &fileInfo ) const;
  QStringList supportedFileNameExtensions() const;
  QStringList supportedMIMETypes() const;
  
signals:
  
  void maxParserIncludeNestingLevelChanged();
  void parserIncludePathsChanged();
  void engineIncludePathsChanged();
  void parserOptionsChanged();
  void engineOptionsChanged();
  void apiNameChanged();
  
public slots:
  
private:
  
  const QScopedPointer<ConfigurationPrivate> d_ptr;
  Q_DECLARE_PRIVATE(Configuration)
  
};

typedef QSharedPointer<Configuration> SharedConfiguration;

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_CONFIGURATION_H
