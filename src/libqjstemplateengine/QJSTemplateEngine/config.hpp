/*
   
    This file is part of QJSTemplateEngine.

    QJSTemplateEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QJSTemplateEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with QJSTemplateEngine.  If not, see <http://www.gnu.org/licenses/>.
  
  
 */

#ifndef QJSTEMPLATEENGINE_CONFIG_HPP
#define QJSTEMPLATEENGINE_CONFIG_HPP

#include <QtGlobal>

#ifdef QJSTEMPLATEENGINE_LIB
#define QJSTEMPLATEENGINE_EXPORT Q_DECL_EXPORT
#else
#define QJSTEMPLATEENGINE_EXPORT Q_DECL_IMPORT
#endif

#endif // CONFIG_HPP

