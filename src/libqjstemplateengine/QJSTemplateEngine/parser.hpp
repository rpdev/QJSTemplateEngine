#ifndef QJSTEMPLATEENGINE_PARSER_H
#define QJSTEMPLATEENGINE_PARSER_H

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Configuration"

#include <QIODevice>
#include <QObject>
#include <QScopedPointer>

namespace QJSTemplateEngine {

// Forward:
class ParserPrivate;

class Parser : public QObject
{
  Q_OBJECT
  
  
public:
  
  enum ParserError {
    NoError,
    ParserIOError,
    ParserOUtOfMemoryError,
    ParserIncludesDisabledError,
    ParserMaxNestingLevelExceededError,
    ParserIncludeFileNotFoundError
  };

  explicit Parser(QObject *parent = 0);
  virtual ~Parser();

  bool parse( const QByteArray &templateString );
  bool parse( QIODevice *device );
  bool parseFile( const QString &fileName );
  const AST::Document* document() const;
  ParserError error() const;
  QString errorString() const;
  SharedConfiguration configuration() const;
  void setConfiguration( const SharedConfiguration &configuration );
  
signals:
  
public slots:
  
private:
  
  const QScopedPointer<ParserPrivate> d_ptr;
  Q_DECLARE_PRIVATE( Parser )

};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_PARSER_H
