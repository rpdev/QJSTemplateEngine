#ifndef QJSTEMPLATEENGINE_AST_H
#define QJSTEMPLATEENGINE_AST_H

#include <QList>
#include <QObject>
#include <QScopedPointer>

namespace QJSTemplateEngine {

/**
  @brief Provides types used to built up the Abstract Syntax Tree of read templates
  
  The classes in the AST namespace are used to represent nodes in the Abstract Syntax Tree into
  which the Parser converts any read template.
 */
namespace AST {

// forward
class Document;
class DocumentPrivate;
class Node;
class NodePrivate;
class TextNode;
class TextNodePrivate;
class JavaScriptNode;
class JavaScriptNodePrivate;
class IncludeNode;
class IncludeNodePrivate;

/**
   @brief Represents a node in the abstract syntaxt tree
   
   This class is the base class for all concrete nodes. It cannot be created. Instead,
   instances of the concrete node objects like Root, TextNode, JavaScriptNode and IncludeNode should
   be created.
 */
class Node : public QObject {
  Q_OBJECT
  
public:
  
  ~Node();
  
protected:
  
  QScopedPointer<NodePrivate> d_ptr;
  
  explicit Node( NodePrivate* d, QObject *parent = 0 );
  
private:
  
  explicit Node( QObject *parent );
  
  Q_DECLARE_PRIVATE( Node )
  
};

/**
   @brief The root of the abstract syntax tree
   
   This class is used to represent the root node of the abstract syntax tree. The Parser
   will produce exactly one such node during parsing a template. The children of the root
   node can either be TextNode, JavaScriptNode or IncludeNode.
 */
class Document : public Node
{
  Q_OBJECT
  
public:
  explicit Document(QObject *parent = 0);
  ~Document();
  
  QList<Node*> childNodes() const;
  TextNode* createTextNode();
  JavaScriptNode* createJavaScriptNode();
  IncludeNode* createIncludeNode();
  
  /**
    @brief Returns all child nodes of a given type
    
    This method will return all child nodes that can be cast to
    the @p Type. 
   */
  template<typename Type>
  QList<Type*> childNodesByType() const {
    QList<Type*> result;
    for ( auto node : childNodes() ) {
      Type *resultNode = dynamic_cast<Type*>( node );
      if ( resultNode != nullptr ) {
        result << resultNode;
      }
    }
    return result;
  }
  
signals:
  
public slots:
  
private:
  Q_DECLARE_PRIVATE( Document )
};

/**
   @brief Represents a text node
   
   This type represents a text node in the AST. Text nodes are text which is printed as-is to the
   output when processing the template afterwards.
 */
class TextNode : public Node {
  Q_OBJECT
  
public:
  explicit TextNode( Node *parent = 0 );
  ~TextNode();
  
  QByteArray text() const;
  void setText( const QByteArray &text );
  
private:
  Q_DECLARE_PRIVATE( TextNode )
};

/**
   @brief Represents a piece of JavaScript in the template
   
   This type is used to represent JavaScript nodes in the template. There are two types of
   JavaScript nodes. Pure processing nodes are surrounded using '<? ... ?>'. These blocks
   will be copied to the resulting JavaScript file as-is. Such blocks will have their print()
   property set to false. The other form of JavaScript nodes is done via '${ ... }'. It is
   assumed that the contained JavaScript results in one result value, which will be written to
   the output stream. Such nodes will have their print() property set to true.
   
 */
class JavaScriptNode : public Node {
  Q_OBJECT
  
public:
  
  explicit JavaScriptNode( Node *parent = 0 );
  ~JavaScriptNode();
  
  QByteArray javaScript() const;
  void setJavaScript( const QByteArray &javaScript );
  
  bool print() const;
  void setPrint( bool print );
  
private:
  
  Q_DECLARE_PRIVATE( JavaScriptNode )

};

/**
   @brief Represents an include node in the template
   
   This class is used to represent an include node in the template. SUch nodes have a file name and
   - after being processed if enabled and the referenced file being found - a list of child nodes
   which can be either TextNode, JavaScriptNode or IncludeNode in turn.
 */
class IncludeNode : public Node {
  Q_OBJECT
  
public:
  
  explicit IncludeNode( Node *parent = 0 );
  ~IncludeNode();
  
  QByteArray fileName() const;
  void setFileName( const QByteArray &fileName );
  
  QList< Node* > childNodes() const;
  TextNode* createTextNode();
  JavaScriptNode* createJavaScriptNode();
  IncludeNode* createIncludeNode();
  
  /**
    @brief Returns all child nodes of a given type
    
    This method will return all child nodes that can be cast to
    the @p Type. 
   */
  template<typename Type>
  QList<Type*> childNodesByType() const {
    QList<Type*> result;
    for ( auto node : childNodes() ) {
      Type *resultNode = dynamic_cast<Type*>( node );
      if ( resultNode != nullptr ) {
        result << resultNode;
      }
    }
    return result;
  }
  
private:
  
  Q_DECLARE_PRIVATE( IncludeNode )
  
};
} // namespace AST

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_AST_H
