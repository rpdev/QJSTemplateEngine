#ifndef QJSTEMPLATEENGINE_PLAINTEXTFILELOADER_H
#define QJSTEMPLATEENGINE_PLAINTEXTFILELOADER_H

#include "QJSTemplateEngine/FileLoader"

#include <QObject>
#include <QScopedArrayPointer>

namespace QJSTemplateEngine {

// Forward declarations:
class PlainTextFileLoaderPrivate;

/**
   @brief FileLoader implementing access to plain text files
   
   The PlainTextFileLoader provides access to plain text files for the template
   engine. It will read provided files line-by-line and make them available
   as a QStringList (which is passed to the template engine via a
   QVariantDataProvider).
   
   Assume a file is loaded like that by the engine:
   
   @code
   engine->addFile( "/home/tux/test.txt" );
   @endcode
   
   Than, in the template, one could use code like that to process
   the file line-wise:
   
   @code
   <?
   // Iterate over the file line-wise:
   var lines = QJSTemplateEngine.files[ "/home/tux/test.txt" ];
   for ( var i = 0; i < lines.length; ++i ) {
     var line = lines[i];
     if ( /^===/.exec( line ) ) {
       // Convert into level 3 heading:
       ?><h3>${line}</h3>
       <?
     } else if ( /^==/.exec( line ) ) {
       // Convert into level 2 heading:
       ?><h2>${line}</h2>
       <?
     } else if ( /^=/.exec( line ) ) {
       // Convert into level 1 heading:
       ?><h1>${line}</h1>
       <?
     } else {
       // Print the line as it is:
       ?>${line}
       <?
     }
   }
   @endcode

   @note The PlainTextFileLoader is a default loader and hence pre-installed
         in each Engine.   
 */
class PlainTextFileLoader : public FileLoader
{
  Q_OBJECT
public:
  
  static const QString Name;
  
  explicit PlainTextFileLoader(QObject *parent = 0);

  // FileLoader interface
  DataProvider *loadFile(const QString &fileName) const override;
  
signals:
  
public slots:
  
private:
  
  Q_DECLARE_PRIVATE(PlainTextFileLoader)
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_PLAINTEXTFILELOADER_H
