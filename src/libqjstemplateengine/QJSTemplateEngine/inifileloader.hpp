#ifndef QJSTEMPLATEENGINE_INIFILEFORMAT_H
#define QJSTEMPLATEENGINE_INIFILEFORMAT_H

#include "QJSTemplateEngine/FileLoader"

#include <QObject>

namespace QJSTemplateEngine {

// Forward declaration
class INIFileLoaderPrivate;

class INIFileLoader : public FileLoader
{
  Q_OBJECT
public:
  
  static const QString Name;
  
  explicit INIFileLoader(QObject *parent = 0);
  
  // FileLoader interface
  DataProvider *loadFile(const QString &fileName) const override;
  
signals:
  
public slots:
  
private:
  
  Q_DECLARE_PRIVATE( INIFileLoader )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_INIFILEFORMAT_H
