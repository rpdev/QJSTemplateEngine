#ifndef QJSTEMPLATEENGINE_FILELOADER_H
#define QJSTEMPLATEENGINE_FILELOADER_H

#include <QList>
#include <QMimeType>
#include <QObject>
#include <QScopedPointer>
#include <QStringList>

namespace QJSTemplateEngine {

// Forward declaration
class FileLoaderPrivate;
class DataProvider;

/**
   @brief Provides functionality to load a file and make its content available to the engine
   
   The FileLoader class is an abstract base class which can be derived from and instances being
   registered in a session in order to allow the template engine accessing certain file types.
   
 */
class FileLoader : public QObject
{
  Q_OBJECT
  
public:
  explicit FileLoader(const QString &name,
                      const QStringList &supportedFileNameExtensions = QStringList(),
                      const QList<QMimeType> &supportedMimeTypes = QList<QMimeType>(),
                      QObject *parent = 0);
  ~FileLoader();

  QString name() const;
  QStringList supportedFileNameExtensions() const;
  QList<QMimeType> suportedMimeTypes() const;
  
  /**
     @brief Loads a file
     
     This method loads the contents of a file specified by the @p fileName and
     returns a DataProvider wrapping the contained data. If loading
     fails, a nullptr is returned.
     
     @note The ownership of the returned DataProvider goes over to the
           caller of the method.
   */
  virtual DataProvider* loadFile( const QString &fileName ) const = 0;
  
protected:
  
  const QScopedPointer<FileLoaderPrivate> d_ptr;
  
  explicit FileLoader( FileLoaderPrivate* d,
                       const QString &name,
                       const QStringList &supportedFileNameExtensions = QStringList(),
                       const QList<QMimeType> &supportedMimeTypes = QList<QMimeType>(),
                       QObject *parent = 0);
  
private:
  
  Q_DECLARE_PRIVATE(FileLoader)

};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_FILELOADER_H
