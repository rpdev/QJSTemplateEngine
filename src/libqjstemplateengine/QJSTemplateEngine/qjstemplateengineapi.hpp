#ifndef QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_H
#define QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_H

#include "QJSTemplateEngine/Engine"

#include <QJSEngine>
#include <QObject>
#include <QScopedPointer>

namespace QJSTemplateEngine {

// Forward
class QJSTemplateEngineAPIPrivate;

/**
   @brief The QJSTemplateEngine JavaScript API
   
   This class provides the JavaScript API used by the JavaScript run
   into the template engine.
 */
class QJSTemplateEngineAPI : public QObject
{
  Q_OBJECT
  
  Q_PROPERTY(QJSValue data READ data NOTIFY dataChanged)
  Q_PROPERTY(QJSValue files READ files NOTIFY filesChanged)
  
public:
  enum APIError {
    NoError,
    APIIOError,
    APIConfigurationError,
    APIJavaScriptParseError,
    APIPermissionError,
    APIInvalidBlock
  };

  explicit QJSTemplateEngineAPI(QObject *parent = 0);
  ~QJSTemplateEngineAPI();
  
  APIError error() const;
  QString errorString() const;
  
  Engine *engine() const;
  void setEngine( Engine *engine );
  
  QIODevice *outputDevice() const;
  void setOutputDevice( QIODevice *outputDevice );
  
  QJSEngine *javaScriptEngine() const;
  void setJavaScriptEngine( QJSEngine *engine );

  QJSValue data() const;
  void setData( const QJSValue &data );
  
  QJSValue files() const;
  void setFiles(const QJSValue &files );
  
  Q_INVOKABLE void print( const QString &data );
  Q_INVOKABLE bool include( const QString &fileName );
  
  Q_INVOKABLE bool addBlock( const QString &blockName, QJSValue block );
  Q_INVOKABLE QJSValue block( const QString &blockName ) const;
  QStringList blocks() const;
  void renderBlocks();
  
signals:
  
  void dataChanged();
  void filesChanged();
  
public slots:
  
private:
  
  QScopedPointer<QJSTemplateEngineAPIPrivate> d_ptr;
  Q_DECLARE_PRIVATE( QJSTemplateEngineAPI )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_H
