#ifndef QJSTEMPLATEENGINE_DATAPROVIDER_H
#define QJSTEMPLATEENGINE_DATAPROVIDER_H

#include <QJSEngine>
#include <QJSValue>
#include <QObject>
#include <QScopedPointer>

namespace QJSTemplateEngine {

// Forward:
class DataProviderPrivate;

/**
   @brief Provides data to the Engine
   
   This class is used to provide data to the Engine on request. Per engine, multiple instances of
   this class can be registered (each under a different name, which is used in the JavaScript to
   refer to the data). On request, the engine will retrieve the data via the data() method, which
   returns a QJSValue (which will be owned by the script engine).
 */
class DataProvider : public QObject
{
  Q_OBJECT
  
public:
  
  explicit DataProvider(QObject *parent = 0);
  ~DataProvider();
  
  /**
     @brief Returns the data wrapped by the provider
     
     This method is used by the engine to retrieve the data wrapped by the
     DataProvider. The returned QJSValue will be owned by the @p scriptEngine (this must be
     considered especially for QJSValues wrapping QObject instances).
     
     Concrete data providers must implement this method to provide some meaningful
     data to the engine.
   */
  virtual QJSValue data( QJSEngine *scriptEngine ) = 0;
  
signals:
  
public slots:
  
protected:
  
  QScopedPointer<DataProviderPrivate> d_ptr;
  
  explicit DataProvider( DataProviderPrivate *d, QObject *parent = 0 );
  
private:
  
  Q_DECLARE_PRIVATE( DataProvider )
  
};

} // namespace QJSTemplateEngine

#endif // QJSTEMPLATEENGINE_DATAPROVIDER_H
