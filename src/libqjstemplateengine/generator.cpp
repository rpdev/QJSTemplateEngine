#include "QJSTemplateEngine/generator.hpp"

#include "generator_p.hpp"

#include <QTextStream>

namespace QJSTemplateEngine {

/**
   @brief Constructor
 */
Generator::Generator(QObject *parent) : QObject(parent),
  d_ptr( new GeneratorPrivate() )
{
}

/**
   @brief Destructor
 */
Generator::~Generator()
{
  
}

/**
   @brief The configuration used by the generator
 */
SharedConfiguration Generator::configuration() const
{
  const Q_D( Generator );
  return d->configuration;
}

/**
   @brief Sets the configuration to be used by the generator
 */
void Generator::setConfiguration(const SharedConfiguration &configuration)
{
  Q_D( Generator );
  d->configuration = configuration;
}

/**
   @brief Transforms a document into JavaScript
   
   This method takes a template @p document and transforms it into JavaScript code. The generated
   code is returned.
 */
QByteArray Generator::generate(const AST::Document *document)
{
  // Note: Keep this non-const in case future revisions want to introduce error codes in the generator
  Q_D( Generator );
  QByteArray result;
  if ( document ) {
    QTextStream stream( &result, QIODevice::WriteOnly );
    d->generate( document, stream );
  }
  return result;
}

} // namespace QJSTemplateEngine

