#include "QJSTemplateEngine/qvariantdataprovider.hpp"

#include "qvariantdataprovider_p.hpp"

namespace QJSTemplateEngine {

QVariantDataProvider::QVariantDataProvider(const QVariant &data, QObject *parent) : 
  DataProvider( new QVariantDataProviderPrivate(), parent)
{
  Q_D( QVariantDataProvider );
  d->data = data;
}

QVariantDataProvider::QVariantDataProvider(QObject *parent) :
  DataProvider( new QVariantDataProviderPrivate(), parent )
{
}

QVariantDataProvider::~QVariantDataProvider()
{
  
}

QVariant QVariantDataProvider::wrappedData() const
{
  const Q_D( QVariantDataProvider );
  return d->data;
}

void QVariantDataProvider::setWrappedData(const QVariant &data)
{
  Q_D( QVariantDataProvider );
  d->data = data;
}

QJSValue QVariantDataProvider::data(QJSEngine *scriptEngine)
{
  Q_D( QVariantDataProvider );
  Q_ASSERT( scriptEngine != nullptr );
  return scriptEngine->toScriptValue( d->data );
}

} // namespace QJSTemplateEngine

