#include "QJSTemplateEngine/configuration.hpp"

#include "configuration_p.hpp"
#include "fileloader.hpp"
#include "plaintextfileloader.hpp"
#include "jsonfileloader.hpp"
#include "inifileloader.hpp"

#include <QFileInfo>
#include <QMimeDatabase>
#include <QQueue>
#include <QSet>

namespace QJSTemplateEngine {

/**
   @brief Constructor
   
   Creates a new Configuration object with the given @p parent.
 */
Configuration::Configuration(QObject *parent) : QObject(parent),
  d_ptr( new ConfigurationPrivate() )
{
  // Register "built-in" file loaders:
  registerFileLoader( new PlainTextFileLoader() ); // Loads plain text files
  registerFileLoader( new JSONFileLoader() ); // Loads JSON formatted files
  registerFileLoader( new INIFileLoader() ); // Loads INI-style files
}

/**
   @brief Destructor
 */
Configuration::~Configuration()
{
  
}

/**
   @brief The maximum parser include nesting level
   
   This variable controls the maximum nesting level of files included by the parser (i.e.
   via the '<?include "fragment.qjst"?>' syntax). Such files may recursively include each other.
   This is wanted (as it might be useful for some templates) but could cause endless inclusion
   loops. By setting this value to a positive number, the maximum inclusion depth can be limited.
   Hence, if an include is encountered, which is at a depth beyond this value, the include will
   fail. A value of 0 effectively disables parser includes. A negative number disables the
   maximum inclusion level check. The default value is -1.
   
   @sa setMaxParserIncludeNestingLevel()
 */
int Configuration::maxParserIncludeNestingLevel() const
{
  const Q_D( Configuration );
  return d->maxParserIncludeNestingLevel;
}

/**
   @brief Sets the maxParserIncludeNestingLevel
 */
void Configuration::setMaxParserIncludeNestingLevel(int maxParserIncludeNestingLevel)
{
  Q_D( Configuration );
  if ( d->maxParserIncludeNestingLevel != maxParserIncludeNestingLevel ) {
    d->maxParserIncludeNestingLevel = maxParserIncludeNestingLevel;
    emit maxParserIncludeNestingLevelChanged();
  }
}

/**
   @brief The list of search paths for includes processed by the parser
   
   This returns the list of include paths considered by the parser. If the Parser encounters
   an '<?include?>' statement, it will search in the listed locations for the file. Only files
   directly located in these locations will be considered (i.e. files further down in sub-
   directories will be ignored).
   
   @sa setParserIncludePaths()
   @sa addParserIncludePath()
 */
QStringList Configuration::parserIncludePaths() const
{
  const Q_D( Configuration );
  return d->parserIncludePaths;
}

/**
   @brief Sets the parser include paths
 */
void Configuration::setParserIncludePaths(const QStringList &parserIncludePaths)
{
  Q_D( Configuration );
  if ( d->parserIncludePaths != parserIncludePaths ) {
    d->parserIncludePaths = parserIncludePaths;
    emit parserIncludePathsChanged();
  }
}

/**
   @brief Adds the @p path to the list of parser include paths
   
   This will add the @p path to the list of parser include paths. If the path either does
   not point to a directory or the path already is registered, the method will fail and return false.
   Otherwise, true is returned.
 */
bool Configuration::addParserIncludePath(const QString &path)
{
  Q_D( Configuration );
  QFileInfo fi( path );
  if ( fi.isDir() ) {
    QString absolutePath = fi.absoluteFilePath();
    if ( !d->parserIncludePaths.contains( absolutePath ) ) {
      d->parserIncludePaths.append( absolutePath );
      emit parserIncludePathsChanged();
      return true;
    }
  }
  return false;
}

/**
   @brief The list of search paths for the Engine
   
   This is the list of search paths scanned by the Engine when resolving includes pulled in via
   QJSTemplateEngine.include(). Only files that are directly located in one of the registered
   directories will be included (i.e. also files in a sub-directory of a registered one will
   be rejected).
   
   @sa setEngineIncludePaths()
   @sa addEngineIncludePath()
 */
QStringList Configuration::engineIncludePaths() const
{
  const Q_D( Configuration );
  return d->engineIncludePaths;
}

/**
   @brief Sets the include paths scanned by the Engine for resolving includes
 */
void Configuration::setEngineIncludePaths(const QStringList &engineIncludePaths)
{
  Q_D( Configuration );
  if ( d->engineIncludePaths != engineIncludePaths ) {
    d->engineIncludePaths = engineIncludePaths;
    emit engineIncludePathsChanged();
  }
}

/**
   @brief Adds a path to the list of engine include paths
   
   This adds the @p path to the list of search paths scanned by the Engine to resolve
   includes. If path points to an existing directory and that directory is not
   yet registered, the path is added and the method returns true. Otherwise, nothing is done
   and false is returned.
 */
bool Configuration::addEngineIncludePath(const QString &path)
{
  Q_D( Configuration );
  QFileInfo fi( path );
  if ( fi.isDir() ) {
    QString absolutePath = fi.absoluteFilePath();
    if ( !d->engineIncludePaths.contains( absolutePath ) ) {
      d->engineIncludePaths.append( absolutePath );
      emit engineIncludePathsChanged();
      return true;
    }
  }
  return false;
}

/**
   @brief The options used for the Parser
 */
Configuration::ParserConfigurationOptions Configuration::parserOptions() const
{
  const Q_D( Configuration );
  return d->parserOptions;
}

/**
   @brief Sets the options used by the parser
 */
void Configuration::setParserOptions(const ParserConfigurationOptions &parserOptions)
{
  Q_D( Configuration );
  if ( d->parserOptions != parserOptions ) {
    d->parserOptions = parserOptions;
    emit parserOptionsChanged();
  }
}

/**
   @brief Enables an individual option for the parser
 */
void Configuration::enableParserOption(Configuration::ParserConfigurationOption option)
{
  Q_D( Configuration );
  setParserOptions( d->parserOptions | option );
}

/**
   @brief Disables an individual option for the parser
 */
void Configuration::disableParserOption(Configuration::ParserConfigurationOption option)
{
  Q_D( Configuration );
  setParserOptions( d->parserOptions & ~option );
}

/**
   @brief Returns the options used for the Engine
 */
Configuration::EngineConfigurationOptions Configuration::engineOptions() const
{
  const Q_D( Configuration );
  return d->engineOptions;
}

/**
   @brief Sets the options to be used by the Engine
 */
void Configuration::setEngineOptions(const EngineConfigurationOptions &engineOptions)
{
  Q_D( Configuration );
  if ( d->engineOptions != engineOptions ) {
    d->engineOptions = engineOptions;
    emit engineOptionsChanged();
  }
}

/**
   @brief Enable an individual @p option for the engine
 */
void Configuration::enableEngineOption(Configuration::EngineConfigurationOption option)
{
  Q_D( Configuration );
  setEngineOptions( d->engineOptions | option );
}

/**
   @brief Disables an individual @p option for the engine
 */
void Configuration::disableEngineOption(Configuration::EngineConfigurationOption option)
{
  Q_D( Configuration );
  setEngineOptions( d->engineOptions & ~option );
}

/**
   @brief Returns the JavaScript API name
   
   This returns the name by which the template engine API can be accessed
   when the JavaScript is run. The default value is 'QJSTemplateEngine'.
 */
QString Configuration::apiName() const
{
  const Q_D( Configuration );
  return d->apiName;
}

/**
   @brief Sets the JavaScript API name
 */
void Configuration::setApiName(const QString &apiName)
{
  Q_D( Configuration );
  if ( d->apiName != apiName ) {
    d->apiName = apiName;
    emit apiNameChanged();
  }
}

/**
   @brief Registers a new FileLoader
   
   This method registers the given @p fileLoader in the configuration.
   Registered FileLoader objects are used in order for the template engine
   to be able to access data stored in external files. If for a particular
   file a FileLoader is registered, then the engine can load data from
   that file on its own.
   
   @note The fileLoader must not be a nullptr. The Configuration takes
         ownership of the registered loader. Registered loaders
         must have unique names (see also FileLoader::name()). If
         a loader with a name which is already used by a previously
         registered loader is added, than the older FileLoader object
         is removed from the configuration and deleted.
 */
void Configuration::registerFileLoader(FileLoader *fileLoader)
{
  Q_D( Configuration );
  Q_CHECK_PTR( fileLoader );
  const QString name = fileLoader->name();
  if ( d->fileLoaders.contains( name ) ) {
    delete d->fileLoaders.value( name );
  }
  d->fileLoaders[ name ] = fileLoader;
  fileLoader->setParent( this );
}

/**
   @brief Returns the file loader with the specified name
   
   This returns a FileLoader which has a FileLoader::name() property
   equal to @p name. If no such loader exists, a nullptr is returned.
 */
const FileLoader *Configuration::fileLoaderByName(const QString &name) const
{
  const Q_D( Configuration );
  return  d->fileLoaders.value( name, nullptr );
}

/**
   @brief Returns a file loader suitable to load files with the given suffix
   
   The method will return a file loader which supports loading files with the
   given file name @p suffix. For this, the loaders' 
   FileLoader::supportedFileNameExtensions() method is used. If not suitable
   loader is found, a nullptr is returned.
 */
const FileLoader *Configuration::fileLoaderByFileNameSuffix(const QString &suffix) const
{
  const Q_D( Configuration );
  for ( auto loader : d->fileLoaders.values() ) {
    if ( loader->supportedFileNameExtensions().contains( suffix.toLower() ) ) {
      return loader;
    }
  }
  return nullptr;
}

/**
   @brief Returns a file loader suitable to load the specified file
   
   This method searches for a loader suitable to load the file specified
   by the @p fileName by checking the FileLoader::suportedMimeTypes()
   of registered file loaders. The MIME type of the file is compared
   to the supported MIME types of the registered loaders; if a match
   is found, the appropriate loader is returned. If no loader
   handles the MIME type of the file, than a nullptr is returned.
 */
const FileLoader *Configuration::fileLoaderByMimeType(
    const QString &fileName) const
{
  QFileInfo fi( fileName );
  return fileLoaderByMimeType( fi );
}

/**
   @brief Returns a file loader suitable to load the specified file
   
   This is an overloaded method. It takes the @p fileInfo for a particular
   file and searches in the list or registered FileLoaders for a suitable
   one for loading this file. If no loader is found, the method returns
   a nullptr.
   
   @note This method will do a search for the most specific registered loader that
         handles the mime type of the file. Consider two loaders are registered -
         one handline text/plain, the other text/xml. The latter derives from the
         former. A search for a loader handling *.xml files would not
         return the loader registered for the text/xml MIME type. However, if only
         a loader for text/plain would be registered, than when a search
         for a loader handling *.xml files is done, this would return the
         loader for text/plain.
 */
const FileLoader *Configuration::fileLoaderByMimeType(
    const QFileInfo &fileInfo) const
{
  const Q_D( Configuration );
  QMimeDatabase mimeDB;
  QQueue<QMimeType> queue;
  QMimeType mimeTypeForFile = mimeDB.mimeTypeForFile( fileInfo );
  if ( mimeTypeForFile.isValid() ) {
    queue.enqueue( mimeTypeForFile );
  }
  while ( !queue.isEmpty() ) {
    mimeTypeForFile = queue.dequeue();
    for ( auto loader : d->fileLoaders.values() ) {    
      for ( auto mimeType : loader->suportedMimeTypes() ) {
        if ( mimeTypeForFile == mimeType ) {
          return loader;
        }
      }
    }
    for ( auto parentMimeType : mimeTypeForFile.parentMimeTypes() ) {
      QMimeType parent = mimeDB.mimeTypeForName( parentMimeType );
      if ( parent.isValid() ) {
        queue.enqueue( parent );
      }
    }
  }
  return nullptr;
}

QStringList Configuration::supportedFileNameExtensions() const
{
  const Q_D( Configuration );
  QSet<QString> result;
  for ( auto loader : d->fileLoaders.values() ) {
    for ( auto extension : loader->supportedFileNameExtensions() ) {
      result.insert( extension );
    }
  }
  return result.values();
}

QStringList Configuration::supportedMIMETypes() const
{
  const Q_D( Configuration );
  QSet<QString> result;
  for ( auto loader : d->fileLoaders.values() ) {
    for ( QMimeType mime : loader->suportedMimeTypes() ) {
      result.insert( mime.name() );
    }
  }
  return result.values();
}


} // namespace QJSTemplateEngine

