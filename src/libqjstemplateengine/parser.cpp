#include "QJSTemplateEngine/parser.hpp"

#include "parser_p.hpp"

#include <QBuffer>
#include <QFile>

namespace QJSTemplateEngine {

/**
   @brief Constructor
 */
Parser::Parser(QObject *parent) : QObject(parent),
  d_ptr( new ParserPrivate() )
{
}

/**
   @brief Destructor
 */
Parser::~Parser()
{ 
}

/**
   @brief Parses a template
   
   This method takes a @p templateString and transforms it into an AST::Document. If an error
   is encountered, this method returns false, otherwise, true is returned.
   
   @sa document()
 */
bool Parser::parse(const QByteArray &templateString)
{
  Q_D( Parser );
  d->clear();
  
  QBuffer buffer;
  buffer.setData( templateString );
  if ( !buffer.open( QIODevice::ReadOnly ) ) {
    d->setParserError( ParserIOError, "Failed to open QBuffer:" + buffer.errorString() );
    return false;
  }
  
  // Delegate to other method:
  return parse( &buffer );
}

/**
   @brief Parses a template document
   
   This is an overloaded method. It reads a template from the given @p device and parses it.
   Returns false in case of an error or true otherwise.
 */
bool Parser::parse(QIODevice *device)
{
  Q_D( Parser );
  d->clear();
  
  AST::Document *root = new AST::Document( this );
  d->ast = root;
  
  if ( !d->parse( device, root ) ) {
    return false;
  }
  
  for ( AST::IncludeNode *include : root->childNodesByType<AST::IncludeNode>() ) {
    if ( !d->parseRecursively( include, 0 ) ) {
      return false;
    }
  }
  
  d->ast = root;
  
  return true;
}

/**
   @brief Parses a template file
   
   This method parses the given @p fileName and transforms it into an AST::Document. If an error
   is encountered, the method returns false, otherwise, true is returned.
 */
bool Parser::parseFile(const QString &fileName)
{
  Q_D( Parser );
  d->clear();
  
  QFile file( fileName );
  if ( !file.open( QIODevice::ReadOnly ) ) {
    d->setParserError( ParserIOError, "Failed to open " + fileName + ": " + file.errorString() );
    return false;
  }
  
  // Delegate to other method:
  return parse( &file );
}

/**
   @brief The document created by a previous parsing operation
   
   This returns the AST::Document which has been created during the most
   recent parse operation (i.e. either calls to parse()  or parseFile()). If no
   parsing happened, this returns a nullptr.
 */
const AST::Document *Parser::document() const
{
  const Q_D( Parser );
  return d->ast;
}

/**
   @brief Returns the last error code that has been encountered by the parser
 */
Parser::ParserError Parser::error() const
{
  const Q_D( Parser );
  return d->error;
}

/**
   @brief Returns the description of the last error encountered by the parser
 */
QString Parser::errorString() const
{
  const Q_D( Parser );
  return d->errorString;
}

/**
   @brief Returns the configuration used by the parser
 */
SharedConfiguration Parser::configuration() const
{
  const Q_D( Parser );
  return d->configuration;
}

/**
   @brief Sets the configuration to be used by the parser
 */
void Parser::setConfiguration(const SharedConfiguration &configuration)
{
  Q_D( Parser );
  d->configuration = configuration;
}

} // namespace QJSTemplateEngine

