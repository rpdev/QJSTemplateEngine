#include "QJSTemplateEngine/qjstemplateengineapi.hpp"

#include "qjstemplateengineapi_p.hpp"

#include <QDir>
#include <QFile>
#include <QFileInfo>

namespace QJSTemplateEngine {

/**
   @brief Constructor
 */
QJSTemplateEngineAPI::QJSTemplateEngineAPI(QObject *parent) : 
  QObject(parent),
  d_ptr( new QJSTemplateEngineAPIPrivate() )
{
}

/**
   @brief Destructor
 */
QJSTemplateEngineAPI::~QJSTemplateEngineAPI()
{
  
}

/**
   @brief Returns the last error encountered by the API
 */
QJSTemplateEngineAPI::APIError QJSTemplateEngineAPI::error() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->error;
}

/**
   @brief Returns the description of the last error encountered by the API
 */
QString QJSTemplateEngineAPI::errorString() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->errorString;
}

/**
   @brief The Engine to which the API provides access to
 */
Engine *QJSTemplateEngineAPI::engine() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->engine;
}

/**
   @brief Sets the Engine to which the API should provide access to
 */
void QJSTemplateEngineAPI::setEngine(Engine *engine)
{
  Q_D( QJSTemplateEngineAPI );
  d->engine = engine;
}

/**
   @brief The output device to render to
   
   This is the device to which any output written via the print() method is written to.
 */
QIODevice *QJSTemplateEngineAPI::outputDevice() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->outputDevice;
}

/**
   @brief Sets the output device to write generated content to
 */
void QJSTemplateEngineAPI::setOutputDevice(QIODevice *outputDevice)
{
  Q_D( QJSTemplateEngineAPI );
  d->outputDevice = outputDevice;
}

/**
   @brief The JavaScript engine which runs the script which is using this API
 */
QJSEngine *QJSTemplateEngineAPI::javaScriptEngine() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->jsEngine;
}

/**
   @brief Sets the JavaScript engine which runs the script which will use this API
 */
void QJSTemplateEngineAPI::setJavaScriptEngine(QJSEngine *engine)
{
  Q_D( QJSTemplateEngineAPI );
  d->jsEngine = engine;
}

/**
   @brief The data property by which registered DataProvier objects can be accessed
   
   This method returns the 'data' object by which registered DataProvider objects can be
   accessed. Consider the following C++ code:
   
   @code
   Engine e;
   // Set up everything...
   DataProvider *myCustomData = createDataProvider( &e );
   e.addDataProvider( "MyData", myCustomData );
   @endcode
   
   A template that later is run could get the registered data via:
   
   @code
   var customData = QJSTemplateEngine.data.MyData;
   @endcode
   
   @sa Engine::addDataProvider()
 */
QJSValue QJSTemplateEngineAPI::data() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->dataProperty;
}

/**
   @brief Sets the 'data' property of the QJSTemplateEngine API exposed to a template
 */
void QJSTemplateEngineAPI::setData(const QJSValue &data)
{
  Q_D( QJSTemplateEngineAPI );
  d->dataProperty = data;
  emit dataChanged();
}

/**
   @brief The files loaded into the engine
   
   This returns the files added to the engine. This is a map where the
   names of the read files are used as keys while the values are the files'
   contents. For example, to process all input files, one would
   iterate over this structure like that:
   
   @code
   <?
   for ( var fileName in QJSTemplateEngine.files ) {
     var contents = QJSTemplateEngine.files[fileName];
     // process files depending e.g. on file name extension:
     if ( /\\.xml$/.exec( fileName ) ) {
       // process the contents of the XML file...
     }
   }
   @endcode
   
   The concrete structure of the file contents depends on the FileLoader
   used to load the file.
   
   @sa FileLoader
   @sa Engine::addFile()
   @sa Engine::addFileBySuffix()
 */
QJSValue QJSTemplateEngineAPI::files() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->filesProperty;
}

/**
   @brief Sets the files property
 */
void QJSTemplateEngineAPI::setFiles(const QJSValue &files)
{
  Q_D( QJSTemplateEngineAPI );
  d->filesProperty = files;
  emit filesChanged();
}

/**
   @brief Add content to the generated output
   
   This method can be used to write content to the default output channel of the template Engine.
   The print() method is used by vast parts of the generated JavaScript (i.e. any text segment
   or printable JavaScript is routed through this method). Additionally, also executed JavaScript
   can use this API to generate content. So instead of:
   
   @code
   <?
    for ( var i = 0; i < 10; ++ i ) {?>
      <li>Entry number ${i+1}</li>
    <?}
   ?>
   @endcode
   
   One can also use this instead:
   
   @code
   <?
    for ( var i = 0; i < 10; ++ i ) {
      QJSTemplateEngine.print( "<li>Entry number " + ( i + 1 ) + "</li>" );
    }
   ?>
   @endcode
   
   In addition, the print() method is the only way to generate content for a JavaScript which
   is included by the Engine (compared to template fragments which are included by the parser).
 */
void QJSTemplateEngineAPI::print(const QString &data)
{
  Q_D( QJSTemplateEngineAPI );
  if ( d->outputDevice != nullptr && d->outputDevice->isWritable() ) {
    d->outputDevice->write( data.toUtf8() );
  }
}

/**
   @brief Includes another JavaScript file
   
   This will include the JavaScript library identified by its @p fileName in the script:
   
   @code
   QJSTemplateEngine.include( "MyJSLib.js" );
   @endcode
   
   The method returns true if the inclusion worked or false in case an error occurred. Typical
   errors are:
   
   - Engine includes are disabled (Configuration::EnableEngineIncludes is not set in the 
     configuration)
   - The parent directory where the included file is located is not registered as engine search
     path (Configuration::addEngineIncludePath())
   - The included JavaScript file has syntax errors or otherwise threw and exception
   
 */
bool QJSTemplateEngineAPI::include(const QString &fileName)
{
  Q_D( QJSTemplateEngineAPI );
  
  // Do nothing if we don't have an engine or a JS engine
  if ( !d->engine ) {
    d->setError( APIConfigurationError, "This API has no Engine" );
    return false;
  }
  if ( !d->jsEngine ) {
    d->setError( APIConfigurationError, "This API has no QJSEngine" );
    return false;
  }
  
  
  // Check if engine side includes are allowed:
  if ( !d->engine->configuration()->engineOptions().testFlag( 
         Configuration::EnableEngineIncludes ) ) {
    d->setError( APIPermissionError, "Engine includes are disabled" );
    return false;
  }
  
  // Search for the file:
  QString absoluteFileName;
  for ( auto path : d->engine->configuration()->engineIncludePaths() ) {
    QDir dir( path );
    QString absPath = dir.absoluteFilePath( fileName );
    QFileInfo fi( absPath );
    if ( fi.exists() ) {
      if ( d->engine->configuration()->engineIncludePaths().contains(
             fi.absoluteDir().absolutePath() ) ) {
        absoluteFileName = absPath;
        break;
      }
    }
  }
  if ( absoluteFileName.isNull() ) {
    d->setError( APIIOError, "File \"" + fileName + "\" not found" );
    return false;
  }
  
  // Try to parse the file:
  QFile file( absoluteFileName );
  if ( !file.open( QIODevice::ReadOnly ) ) {
    d->setError( APIIOError, "Failed to open \"" + absoluteFileName + "\": " +
                 file.errorString() );
    return false;
  }
  auto result = d->jsEngine->evaluate( file.readAll(), absoluteFileName );
  if ( result.isError() ) {
    d->setError( APIJavaScriptParseError, "Error when processing \"" +
                 absoluteFileName + "\": " + result.toString() );
    return false;
  }
  
  return true;
}

/**
   @brief Creates or overrides a block
   
   This method can be used to create a block. Blocks are rendered after the
   actual template has run. A block has a name and a definition which must
   include at least a render() method. A minimal template which is
   using a block could look like this:
   
   @code
   <?
   QJSTemplateEngine.addBlock( 
     "my_block",
     {
       render: function() {
         ?>Hello World!<?
       }
     });
   @endcode
   
   This would result in the following output being generated:
   
   @code
   Hello World!
   @endcode
   
   Blocks are executed after the actual template has run. That means, that any
   inline text or interpolations that appear outside a block appear first in the
   generated output while everything produced by a block appears at the end
   of the output. Considering the following template:
   
   @code
   <?
   QJSTemplateEngine.addBlock( 
     "my_block",
     {
       render: function() {
         ?>Hello World!<?
       }
     });
   
   ?>Header Text
   
   @endcode
   
   As blocks are exectured after the actual template has run, this will
   produce the following output:
   
   @code
   Header Text
   Hello World
   @endcode
   
   For simple code generation tasks, blocks are not required and using
   inline text and interpolations is easier to use. However, blocks
   can be used for template specialization. Consider the following base template:
   
   @code
   <?
   QJSTemplateEngine.addBlock( 
     "header", // The name of the block
     {
       render: function() {
         ?>${this.commentStart}
   <?
         this.lines.forEach( function(line) {
           ?>${this.commentPrefix}${line}
   <?
         });
         ?>${this.commentEnd}
   <?
        },
        commentStart: "",
        commentEnd: "",
        commentPrefix: "",
        lines: [
            "Copyright Header",
            "(c) Some Company"
        ]
    });
   @endcode
   
   Rendering this template as it is would result in the following output:
   
   @code
   Copyright Header
   (c) Some Company
   @endcode
   
   However, one can also use this template as a base:
   
   @code
   <?include "base.qjst"?><?
   
   // Get the header block defined in the base template:
   var header = QJSTemplateEngine.block( "header" );
   
   // Specialize it, e.g. change to C++ style comments:
   header.commentStart  = "//";
   header.commentPrefix = "// ";
   header.commentEnd    = "//";
   
   // Override the previously registered block:
   QJSTemplateEngine.addBlock( "header", header );
   @endcode
   
   Running this template would result in the following output:
   
   @code
   //
   // Copyright Header
   // (c) Some Company
   //
   @endcode
   
   @note Using addBlock() within the render() method of a block itself has
         no effect.
 */
bool QJSTemplateEngineAPI::addBlock(const QString &blockName, QJSValue block)
{
  Q_D( QJSTemplateEngineAPI );
  
  // Disable block adding/overriding while we render blocks:
  if ( d->renderingBlocks ) {
    return false;
  }
  
  // Blocks must have render() methid:
  if ( !block.hasProperty( "render" ) && !block.property("render").isCallable() ) {
    d->setError( APIInvalidBlock, 
                 QString( "Error adding block %1: Block definition has no render() method" )
                 .arg( blockName ) );
    return false;
  }
  
  // Check if the block is "new":
  if ( !d->blocks.contains( blockName ) ) {
    // If so, add it to the end of the list of blocks:
    d->blockNames << blockName;
  }
  d->blocks.insert( blockName, block );
  return true;
}

/**
   @brief Returns a block definition
   
   Returns the definition of the block with the given @p blockName. If no
   such block has been registered previously, returns an invalid
   QJSValue.
 */
QJSValue QJSTemplateEngineAPI::block(const QString &blockName) const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->blocks.value( blockName );
}

/**
   @brief Returns the registred blocks
   
   Returns the list of block names that have been registered using
   addBlock(). The order or block names corresponds to the order in which
   the blocks have been registered (and consequentially, 
   in which they are executed).
 */
QStringList QJSTemplateEngineAPI::blocks() const
{
  const Q_D( QJSTemplateEngineAPI );
  return d->blockNames;
}

void QJSTemplateEngineAPI::renderBlocks()
{
  Q_D( QJSTemplateEngineAPI );
  d->renderingBlocks = true;
  for ( QString blockName : d->blockNames ) {
    QJSValue block = d->blocks.value( blockName );
    block.property( "render" ).callWithInstance( block );
  }
  d->renderingBlocks = false;
}

} // namespace QJSTemplateEngine

