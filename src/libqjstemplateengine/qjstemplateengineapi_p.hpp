#ifndef QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_P_HPP
#define QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_P_HPP

#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/QJSTemplateEngineAPI"

#include <QDebug>
#include <QHash>
#include <QIODevice>
#include <QJSEngine>

namespace QJSTemplateEngine {

struct QJSTemplateEngineAPIPrivate {

  QJSTemplateEngineAPI::APIError error;
  QString errorString;
  Engine *engine;
  QIODevice *outputDevice;
  QJSEngine *jsEngine;
  QJSValue dataProperty;
  QJSValue filesProperty;
  QHash<QString,QJSValue> blocks;
  QStringList blockNames;
  bool renderingBlocks;
  
  
  QJSTemplateEngineAPIPrivate() :
    error( QJSTemplateEngineAPI::NoError ),
    errorString(),
    engine( nullptr ),
    outputDevice( nullptr ),
    jsEngine( nullptr ),
    dataProperty(),
    filesProperty(),
    blocks(),
    renderingBlocks( false ) {
  }
  
  void setError( QJSTemplateEngineAPI::APIError error, const QString &errorString ) {
    this->error = error;
    this->errorString = errorString;
    qWarning() << "QJSTemplateEngine::QJSTemplateEngineAPI:" << errorString;
  }
    
};

}

#endif // QJSTEMPLATEENGINE_QJSTEMPLATEENGINEAPI_P_HPP

