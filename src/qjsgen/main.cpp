#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/Parser"
#include "QJSTemplateEngine/Generator"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>

#include <iostream>

#include <cstdio>

using namespace QJSTemplateEngine;

class CLI : public QObject {
  
public:
  explicit CLI( const QStringList &arguments );
  int run();
  
private:
  QStringList         m_arguments;
  QCommandLineParser  m_parser;
  QCommandLineOption  m_helpOption;
  QCommandLineOption  m_versionOption;
  QCommandLineOption  m_templateOption;
  QCommandLineOption  m_compiledTemplateOption;
  QCommandLineOption  m_outputOption;
  QCommandLineOption  m_parserIncludePath;
  QCommandLineOption  m_engineIncludePath;
  QCommandLineOption  m_dataFileOption;
  QCommandLineOption  m_dataFileByExtensionOption;
  
  bool validateCommandLine();
  bool transpile();
  bool generate();
};

int main( int argc, char **argv ) {
  QCoreApplication::setApplicationName( "qjsgen" );
  QCoreApplication::setApplicationVersion( VERSION );
  QCoreApplication::setOrganizationName( "RPdev" );
  QCoreApplication::setOrganizationDomain( "rpdev.net" );
  
  QCoreApplication app( argc, argv );
  CLI cli( app.arguments() );
  return cli.run();
}

CLI::CLI(const QStringList &arguments) :
  m_arguments( arguments ),
  m_parser(),
  m_helpOption( m_parser.addHelpOption() ),
  m_versionOption( m_parser.addVersionOption() ),
  m_templateOption( { "t", "template" }, tr( "The template <file> to process. Must either "
                                             "be a *.qjst file or a *.js file." ), tr( "file" ) ),
  m_compiledTemplateOption( { "c", "compiled-template" }, tr( "Write the compiled JavaScript "
                                                              "into <file>. If this option is "
                                                              "given, then the input file "
                                                              "specified via -t must be a "
                                                              "*.qjst file." ), tr( "file" ) ),
  m_outputOption( { "o", "output" }, tr( "Writes the generated output to <file>. "
                                         "If omitted, output is written to STDOUT." ), 
                  tr( "file" ) ),
  m_parserIncludePath( { "I", "template-include-path" }, tr( "Adds the <path> to the list of "
                                                             "template search paths. Only if "
                                                             "a template is directly contained "
                                                             "within one of the listed search "
                                                             "paths, including it via "
                                                             "<?include ...?> will succeed. This "
                                                             "option can be used multiple times "
                                                             "in order to add several include "
                                                             "paths." ),
                       tr( "path" ) ),
  m_engineIncludePath( { "L", "engine-include-path" }, tr( "Adds a JavaScript library search "
                                                           "<path>. Only libraries contained "
                                                           "directly within one of the added "
                                                           "engine search "
                                                           "path can be included via the "
                                                           "QJSTemplateEngine.include() API. This "
                                                           "option can be used multiple times "
                                                           "in order to add several include paths. "),
                       tr( "path" ) ),
  m_dataFileOption( { "f", "data-file" }, tr( "Loads the <file> and makes it available via the "
                                              "QJSTemplateEngine.files map (with the <file> as key "
                                              "and the loaded data as value). The way the file "
                                              "is opened and parsed is determined by the MIME "
                                              "type of the file. The following MIME types are "
                                              "supported: " ), tr( "file" ) ),
  m_dataFileByExtensionOption( { "F", "data-file-by-extension" }, 
                               tr( "Loads the <file> and makes it available via the "
                                   "QJSTemplateEngine.files map (with the <file> as key "
                                   "and the loaded data as value). The way the file "
                                   "is opened and parsed is determined by the file name "
                                   "extension of the file. The following file name extensions are "
                                   "supported: " ), tr( "file" ) )
{
  Configuration configuration;
  m_dataFileOption.setDescription( m_dataFileOption.description() +
                                   configuration.supportedMIMETypes().join( ", " ) );
  m_dataFileByExtensionOption.setDescription( m_dataFileByExtensionOption.description() +
                                              configuration.supportedFileNameExtensions().join( ", " ) );
  m_parser.setApplicationDescription( 
        tr( "A code generator tool using JavaScript style files as template language." ));
  m_parser.addOption( m_templateOption );
  m_parser.addOption( m_compiledTemplateOption );
  m_parser.addOption( m_outputOption );
  m_parser.addOption( m_parserIncludePath );
  m_parser.addOption( m_engineIncludePath );
  m_parser.addOption( m_dataFileOption );
  m_parser.addOption( m_dataFileByExtensionOption );
}

int CLI::run()
{
  m_parser.process( m_arguments );
  
  // Check if user requested transpilation of template to JS
  if ( transpile() ) {
    return 0;
  }
  
  // Check if user requested generation:
  if ( generate() ) {
    return 0;
  }
  
  return 1;
}

bool CLI::validateCommandLine()
{
  return true;
  // TODO: Do we need some kind of checking here?
}

bool CLI::transpile()
{
  
  if ( m_parser.isSet( m_compiledTemplateOption ) ) {
    QFile input;
    QString jsFile = m_parser.value( m_compiledTemplateOption );
    if ( m_parser.isSet( m_templateOption ) ) {
      QString templateFile = m_parser.value( m_templateOption );
      if ( !templateFile.toLower().endsWith( ".qjst" ) ) {
        qFatal( "Unknown template file format of file %s for transpilation",
                qUtf8Printable( templateFile ) );
      }
      input.setFileName( templateFile );
      if ( !input.open( QIODevice::ReadOnly ) ) {
        qFatal( "Failed to open template file %s: %s",
                qUtf8Printable( templateFile ), qUtf8Printable( input.errorString() ) );
      }
    } else {
      if ( !input.open( stdin, QIODevice::ReadOnly ) ) {
        qFatal( "Failed to open stdin: %s", qUtf8Printable( input.errorString() ) );
      }
    }
    
    
    Parser parser;
    if ( !parser.parse( &input ) ) {
      qFatal( "Failed to parse template: %s", qUtf8Printable( parser.errorString() ) );
    }
    
    Generator generator;
    QFile output( jsFile );
    if ( !output.open( QIODevice::WriteOnly ) ) {
      qFatal( "Failed to open %s for writing: %s", 
              qUtf8Printable( jsFile ), 
              qUtf8Printable( output.errorString() ) );
    }
    
    output.write( generator.generate( parser.document() ) );
    output.close();
    return true;
  }
  return false;
}

bool CLI::generate()
{
  Engine engine;
  QFile input;
  QFile output;
  bool inputIsCompiledTemplate = false;
  
  if ( m_parser.isSet( m_templateOption ) ) {
    QString templateFile = m_parser.value( m_templateOption );
    inputIsCompiledTemplate = !templateFile.toLower().endsWith( ".qjst" );
    input.setFileName( templateFile );
    if ( !input.open( QIODevice::ReadOnly ) ) {  
      qFatal( "Failed to input file %s: %s", 
              qUtf8Printable( templateFile ),
              qUtf8Printable( input.errorString() ) );
    }
  } else {
    if ( !input.open( stdin, QIODevice::ReadOnly ) ) {
      qFatal( "Failed to open stdin: %s", qUtf8Printable( input.errorString() ) );
    }
  }
  
  if ( m_parser.isSet( m_outputOption ) ) {
    QString fileName = m_parser.value( m_outputOption );
    output.setFileName( fileName );
    if ( !output.open( QIODevice::WriteOnly ) ) {
      qFatal( "Failed to open output file %s: %s", 
              qUtf8Printable( fileName ),
              qUtf8Printable( output.errorString() ) );
    }
  } else {
    if ( !output.open( stdout, QIODevice::WriteOnly ) ) {
      qFatal( "Failed to open stdout: %s", qUtf8Printable( output.errorString() ) );
    }
  }
  
  for ( auto path : m_parser.values( m_parserIncludePath ) ) {
    engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
    engine.configuration()->addParserIncludePath( path );
  }
  for ( auto path : m_parser.values( m_engineIncludePath ) ) {
    engine.configuration()->enableEngineOption( Configuration::EnableEngineIncludes );
    engine.configuration()->addEngineIncludePath( path );
  }
  
  for ( auto file : m_parser.values( m_dataFileOption ) ) {
    if ( !engine.addFile( file ) ) {
      qFatal( "Failed to load %s by MIME type: %s", 
              qUtf8Printable( file ), qUtf8Printable( engine.errorString() ) );
    }
  }
  for ( auto file : m_parser.values( m_dataFileByExtensionOption ) ) {
    if ( !engine.addFileBySuffix( file ) ) {
      qFatal( "Failed to load %s by file name extension: %s",
              qUtf8Printable( file ), qUtf8Printable( engine.errorString() ) );
    }
  }
  
  if ( ! ( ( inputIsCompiledTemplate && engine.renderJavaScript( &input, &output ) ) ||
           engine.render( &input, &output ) ) ) {
    qFatal( "Failed to render: %s", qUtf8Printable( engine.errorString() ) );
  }
  
  return true;
}
