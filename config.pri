VERSION = 0.0.0
QMAKE_TARGET_COMPANY = RPdev
QMAKE_TARGET_PRODUCT = QJSTemplateEngine
QMAKE_TARGET_DESCRIPTION = A JavaScript based template engine for Qt
QMAKE_TARGET_COPYRIGHT = (c) RPdev 2015

# Make version available during compilation:
DEFINES += VERSION=\\\"$$VERSION\\\"


# Enable C++11 features:
CONFIG += c++11

# Treat all warnings as errors:
QMAKE_CXXFLAGS += -Werror

# Add QJSTEMPLATEENGINE_DEBUG symbol when building in debug mode:
CONFIG(debug, release|debug):DEFINES += QJSTEMPLATEENGINE_DEBUG

# Setup installation paths (can be overridden when calling qmake):
isEmpty(PREFIX) {
    PREFIX = /usr/local
}
isEmpty(BIN_PATH) {
    BIN_PATH = $$PREFIX/bin
}
isEmpty(LIB_DIR) {
    QT_INSTALL_LIBS = $$[QT_INSTALL_LIBS]
    LIB_DIR = $$basename(QT_INSTALL_LIBS)
}
isEmpty(LIB_PATH) {
    LIB_PATH = $$PREFIX/$$LIB_DIR
}
isEmpty(HEADER_PATH){
    HEADER_PATH = $$PREFIX/include
}
message("Configuration:")
message("  PREFIX:       $$PREFIX")
message("  BIN_PATH:     $$BIN_PATH")
message("  LIB_PATH:     $$LIB_PATH")
message("  HEADER_PATH:  $$HEADER_PATH")

# Define function to setup libraries:
defineTest(setupLibrary) {
    name = $$1

    TEMPLATE = lib
    TARGET = $$name

    DEFINES += $$upper(name)_LIB

    target.path = $$LIB_PATH
    INSTALLS += target

    DESTDIR = $$OUT_PWD/../../bin
    
    export(TEMPLATE)
    export(TARGET)
    export(DEFINES)
    export(target)
    export(INSTALLS)
    export(DESTDIR)
}

# Define function to setup applications:
defineTest(setupApplication) {
    name = $$1

    TEMPLATE = app
    TARGET = $$name

    target.path = $$BIN_PATH
    INSTALLS += target

    DESTDIR = $$OUT_PWD/../../bin

    LIBS += -L"$$DESTDIR" -lqjstemplateengine

    INCLUDEPATH = $$PWD/../libqjstemplateengine

    export(TEMPLATE)
    export(TARGET)
    export(target)
    export(INSTALLS)
    export(DESTDIR)
    export(LIBS)
    export(INCLUDEPATH)
}

# Define function to setup unit tests:
defineTest(setupUnitTest) {
    name = $$1

    TEMPLATE = app
    TARGET = tst_$$name
    CONFIG += console testcase
    CONFIG -= app_bundle
    QT += testlib
    QT -= gui
    DEFINES += SRCDIR=\\\"$$PWD\\\"
    
    LIBS += -L"$$OUT_PWD/../../bin" -lqjstemplateengine

    DESTDIR = $$OUT_PWD/../../bin

    INCLUDEPATH += $$PWD/../../src/libqjstemplateengine

    export(TEMPLATE)
    export(TARGET)
    export(CONFIG)
    export(QT)
    export(DEFINES)
    export(LIBS)
    export(DESTDIR)
    export(INCLUDEPATH)
}
