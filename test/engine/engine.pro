include(../../config.pri)
setupUnitTest(engine)

SOURCES += tst_enginetest.cpp

DISTFILES += \
    hello.qjst \
    hello.js \
    templates/header.qjst \
    js/lib.js \
    complex.qjst \
    test.txt \
    identity.qjst \
    apis/inheritance/base.qjst \
    apis/inheritance/derived.qjst \
    apis/inheritance/expected_result.cpp.txt \
    apis/blocks/template.qjst \
    apis/blocks/expected_output.txt
