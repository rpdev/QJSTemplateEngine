#include <QString>
#include <QtTest>

#include <QByteArray>
#include <QBuffer>

#include "QJSTemplateEngine/Engine"

using namespace QJSTemplateEngine;

class EngineTest : public QObject
{
  Q_OBJECT
  
public:
  EngineTest();
  
private:
  static const QByteArray SimpleTemplate;
  static const QByteArray SimpleJavaScript;
  static const QByteArray ExpectedSimpleOutput;
  static const QString HelloTemplate;
  static const QString HelloJavaScript;
  static const QString ComplexTemplate;
  static const QByteArray ExpectedOutputFromHelloTemplate;
  static const QByteArray ExpectedOutputFromComplex;
  
private Q_SLOTS:
  void defaultConfiguration();
  void renderFromASTToDevice();
  void renderFromASTToByteArray();
  void renderFromDeviceToDevice();
  void renderFromDeviceToByteArray();
  void renderFromByteArrayToDevice();
  void renderFromByteArrayToByteArray();
  void renderFileToDevice();
  void renderFileToByteArray();
  void renderJavaScriptFromDeviceToDevice();
  void renderJavaScriptFromDeviceToByteArray();
  void renderJavaScriptFromByteArrayToDevice();
  void renderJavaScriptFromByteArrayToByteArray();
  void renderJavaScriptFileToDevice();
  void renderJavaScriptFileToByteArray();
  void renderPlainText();
  void renderExecutableJavaScript();
  void renderPrintableJavaScript();
  void renderParserIncludeAccessDenied();
  void renderParserIncludePathNotFound();
  void renderEngineIncludeAccessDenied();
  void renderEngineIncludePathNotFound();
  void renderEngineComplex();
  void addFile();
  void addFileByFileNameExtension();
  void simpleBlock();
  void templateSpecialization();
};

const QByteArray EngineTest::SimpleTemplate = "Hello World";
const QByteArray EngineTest::SimpleJavaScript = "QJSTemplateEngine.print(\"Hello World\");";
const QByteArray EngineTest::ExpectedSimpleOutput = "Hello World";
const QString EngineTest::HelloTemplate = SRCDIR "/hello.qjst";
const QString EngineTest::HelloJavaScript = SRCDIR "/hello.js";
const QString EngineTest::ComplexTemplate = SRCDIR "/complex.qjst";
const QByteArray EngineTest::ExpectedOutputFromHelloTemplate = "Hello World\n";
const QByteArray EngineTest::ExpectedOutputFromComplex =
    "\n==========\n\nHello Mr. World!\nMr. World\n";

EngineTest::EngineTest()
{
}

void EngineTest::defaultConfiguration()
{
  Engine engine;
  QVERIFY( !engine.configuration().isNull() );
}

void EngineTest::renderFromASTToDevice()
{
  Parser p;
  QVERIFY2( p.parse( SimpleTemplate ), "Failed to parse" );
  
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open QBuffer" );
  
  Engine engine;
  QVERIFY2( engine.render( p.document(), &buffer ), "Failed to render" );
  
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderFromASTToByteArray()
{
  Parser p;
  QVERIFY2( p.parse( SimpleTemplate ), "Failed to parse" );
  
  QByteArray buffer;
  
  Engine engine;
  QVERIFY2( engine.render( p.document(), buffer ), "Failed to render" );
  
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderFromDeviceToDevice()
{
  QBuffer input;
  input.setData( SimpleTemplate );
  QVERIFY2( input.open( QIODevice::ReadOnly ), "Failed to open input device" );
  
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.render( &input, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderFromDeviceToByteArray()
{
  QBuffer input;
  input.setData( SimpleTemplate );
  QVERIFY2( input.open( QIODevice::ReadOnly ), "Failed to open input device" );
  
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.render( &input, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderFromByteArrayToDevice()
{
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.render( SimpleTemplate, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderFromByteArrayToByteArray()
{
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.render( SimpleTemplate, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderFileToDevice()
{
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.renderFile( HelloTemplate, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedOutputFromHelloTemplate );
}

void EngineTest::renderFileToByteArray()
{
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.renderFile( HelloTemplate, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedOutputFromHelloTemplate );
}

void EngineTest::renderJavaScriptFromDeviceToDevice()
{
  QBuffer input;
  input.setData( SimpleJavaScript );
  QVERIFY2( input.open( QIODevice::ReadOnly ), "Failed to open input device" );
  
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.renderJavaScript( &input, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderJavaScriptFromDeviceToByteArray()
{
  QBuffer input;
  input.setData( SimpleJavaScript );
  QVERIFY2( input.open( QIODevice::ReadOnly ), "Failed to open input device" );
  
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.renderJavaScript( &input, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderJavaScriptFromByteArrayToDevice()
{
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.renderJavaScript( SimpleJavaScript, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderJavaScriptFromByteArrayToByteArray()
{
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.renderJavaScript( SimpleJavaScript, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderJavaScriptFileToDevice()
{
  QByteArray ba;
  QBuffer buffer( &ba );
  QVERIFY2( buffer.open( QIODevice::WriteOnly ), "Failed to open output device" );
  
  Engine engine;
  QVERIFY2( engine.renderJavaScriptFile( HelloJavaScript, &buffer ), "Failed to render" );
  QCOMPARE( ba, ExpectedSimpleOutput );
}

void EngineTest::renderJavaScriptFileToByteArray()
{
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.renderJavaScriptFile( HelloJavaScript, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedSimpleOutput );
}

void EngineTest::renderPlainText()
{
  const QByteArray PlainText = "Hello World";
  const QByteArray ExpectedOutput = "Hello World";
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.render( PlainText, buffer ), "Failed to render" );
  QCOMPARE( PlainText, ExpectedOutput );
}

void EngineTest::renderExecutableJavaScript()
{
  const QByteArray PlainText = "<? for ( var i = 0; i < 3; ++i ) { ?>Hello World!\n<? } ?>";
  const QByteArray ExpectedOutput = "Hello World!\nHello World!\nHello World!\n";
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.render( PlainText, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedOutput );
}

void EngineTest::renderPrintableJavaScript()
{
  const QByteArray PlainText = "Hello ${\"Mr. World\"}";
  const QByteArray ExpectedOutput = "Hello Mr. World";
  QByteArray buffer;
  Engine engine;
  QVERIFY2( engine.render( PlainText, buffer ), "Failed to render" );
  QCOMPARE( buffer, ExpectedOutput );
}

void EngineTest::renderParserIncludeAccessDenied()
{
  QByteArray output;
  Engine engine;
  // Must fail as the file includes another file which is disabled:
  QTest::ignoreMessage(
        QtWarningMsg, 
        QRegularExpression( "Found include statement but parser includes are disabled" ) );
  QTest::ignoreMessage(
        QtWarningMsg, 
        QRegularExpression( "Found include statement but parser includes are disabled" ) );
  QCOMPARE( engine.renderFile( ComplexTemplate, output ), false );
  QCOMPARE( engine.error(), Engine::EngineParserError );
}

void EngineTest::renderParserIncludePathNotFound()
{
  QByteArray output;
  Engine engine;
  engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  // Must fail as there is no include path set:
  QTest::ignoreMessage( 
        QtWarningMsg,
        QRegularExpression( "The referenced parser include file header\\.qjst could not be found" ));
  QTest::ignoreMessage( 
        QtWarningMsg,
        QRegularExpression( "The referenced parser include file header\\.qjst could not be found" ));
  QCOMPARE( engine.renderFile( ComplexTemplate, output ), false );
  QCOMPARE( engine.error(), Engine::EngineParserError );
}

void EngineTest::renderEngineIncludeAccessDenied()
{
  QByteArray output;
  Engine engine;
  engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  engine.configuration()->addParserIncludePath( SRCDIR "/templates" );
  // Must fail as engine includes are not enabled:
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "Engine includes are disabled" ) );
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "Engine includes are disabled" ) );
  QCOMPARE( engine.renderFile( ComplexTemplate, output ), false );
  QCOMPARE( engine.error(), Engine::EngineJavaScriptError );
}

void EngineTest::renderEngineIncludePathNotFound()
{
  QByteArray output;
  Engine engine;
  engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  engine.configuration()->addParserIncludePath( SRCDIR "/templates" );
  engine.configuration()->enableEngineOption( Configuration::EnableEngineIncludes );
  // Must fail as no include path is registered:
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "File .* not found" ) );
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "File .* not found" ) );
  QCOMPARE( engine.renderFile( ComplexTemplate, output ), false );
  QCOMPARE( engine.error(), Engine::EngineJavaScriptError );
}

void EngineTest::renderEngineComplex()
{
  QByteArray output;
  Engine engine;
  engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  engine.configuration()->addParserIncludePath( SRCDIR "/templates" );
  engine.configuration()->enableEngineOption( Configuration::EnableEngineIncludes );
  engine.configuration()->addEngineIncludePath( SRCDIR "/js" );
  // Must succeed:
  QVERIFY2( engine.renderFile( ComplexTemplate, output ), "Failed to render" );
  QCOMPARE( output, ExpectedOutputFromComplex );
}

void EngineTest::addFile()
{
  Engine engine;
  QVERIFY2( engine.addFile( SRCDIR "/test.txt" ), 
            "Failed to load test.txt sample file!" );
  
  QByteArray reference;
  QFile file( SRCDIR "/test.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ), "Failed to open input file" );
  reference = file.readAll();
  file.close();
  
  QByteArray output;
  engine.renderFile( SRCDIR "/identity.qjst", output );
  
  QCOMPARE( output, reference );
}

void EngineTest::addFileByFileNameExtension()
{
  Engine engine;
  QVERIFY2( engine.addFileBySuffix( SRCDIR "/test.txt" ), 
            "Failed to load test.txt sample file!" );
  
  QByteArray reference;
  QFile file( SRCDIR "/test.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ), "Failed to open input file" );
  reference = file.readAll();
  file.close();
  
  QByteArray output;
  engine.renderFile( SRCDIR "/identity.qjst", output );
  
  QCOMPARE( output, reference );
}

void EngineTest::simpleBlock()
{
  Engine engine;
  QByteArray output;
  QVERIFY2( engine.renderFile( SRCDIR "/apis/blocks/template.qjst", output ),
            "Failed to render template" );
  
  QFile file( SRCDIR "/apis/blocks/expected_output.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ), 
            "Failed to open expected_output.txt" );
  QCOMPARE( output, file.readAll() );
}

void EngineTest::templateSpecialization()
{
  Engine engine;
  engine.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  engine.configuration()->addParserIncludePath( SRCDIR "/apis/inheritance" );
  QByteArray output;
  QVERIFY2( engine.renderFile( SRCDIR "/apis/inheritance/derived.qjst", output ),
            "Failed to render template" );
  QFile file( SRCDIR "/apis/inheritance/expected_result.cpp.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ), "Failed to open reference file" );
  QByteArray reference = file.readAll();
  QCOMPARE( output, reference );
}

QTEST_APPLESS_MAIN(EngineTest)

#include "tst_enginetest.moc"
