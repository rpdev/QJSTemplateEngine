include(../../config.pri)
setupUnitTest(inifileloader)

SOURCES += tst_inifileloadertest.cpp

DISTFILES += \
    data.ini \
    expected_output.txt \
    template.qjst

