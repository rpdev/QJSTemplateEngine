#include "QJSTemplateEngine/Configuration"
#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/FileLoader"
#include "QJSTemplateEngine/INIFileLoader"

#include <QFile>
#include <QString>
#include <QtTest>

using namespace QJSTemplateEngine;

class INIFileLoaderTest : public QObject
{
  Q_OBJECT
  
public:
  INIFileLoaderTest();
  
private Q_SLOTS:
  void iniSupportedByDefault();
  void loadIniByFileNameExtension();
  void iniStructure();
};

INIFileLoaderTest::INIFileLoaderTest()
{
}

void INIFileLoaderTest::iniSupportedByDefault()
{
  Configuration configuration;
  const FileLoader *loader = configuration.fileLoaderByFileNameSuffix( "ini" );
  Q_CHECK_PTR( loader );
  QCOMPARE( loader->name(), INIFileLoader::Name );
}

void INIFileLoaderTest::loadIniByFileNameExtension()
{
  Engine engine;
  QVERIFY2( engine.addFileBySuffix( SRCDIR "/data.ini" ),
            "Failed to load INI file by file name extension" );
}

void INIFileLoaderTest::iniStructure()
{
  Engine engine;
  QVERIFY2( engine.addFileBySuffix( SRCDIR "/data.ini" ),
            "Failed to load data.ini" );
  QByteArray output;
  QVERIFY2( engine.renderFile( SRCDIR "/template.qjst", output ),
            "Failed to render template.qjst" );
  QFile file( SRCDIR "/expected_output.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ),
            "Failed to open expected_output.txt" );
  QCOMPARE( output, file.readAll() );
}

QTEST_APPLESS_MAIN(INIFileLoaderTest)

#include "tst_inifileloadertest.moc"
