#include "QJSTemplateEngine/Parser"

#include <QBuffer>
#include <QString>
#include <QtTest>


using namespace QJSTemplateEngine;

class ParserTest : public QObject
{
  Q_OBJECT
  
public:
  ParserTest();
  
private Q_SLOTS:
  
  void defaultConfiguration();
  void simpleTextNode();
  void simpleJavaScriptNode();
  void simpleJavaScriptPrintedNode();
  void simpleIncludeNode();
  void loadFromDevice();
  void loadFromFile();
  void complexExample();
  void openJavaScriptBlock();
  void openJavaScriptPrintBlock();
  void nonExistingFile();
};



ParserTest::ParserTest()
{
}

void ParserTest::defaultConfiguration()
{
  Parser parser;
  // Configuration must exist:
  QVERIFY( !parser.configuration().isNull() );
  // Parser includes should be disabled by default:
  QVERIFY( !parser.configuration()->parserOptions().testFlag( 
             Configuration::EnableParserIncludes ) );
  
  // Engine includes should be disabled by default:
  QVERIFY( !parser.configuration()->engineOptions().testFlag( 
             Configuration::EnableEngineIncludes ) );
}

void ParserTest::simpleTextNode()
{
  const QByteArray Template = "Hello World!";
  Parser p;
  
  // parse must return true:
  QVERIFY( p.parse( Template ) );
  
  // No error code:
  QCOMPARE( p.error(), Parser::NoError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // There must be one node only:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  
  // There must be exactly one Text child node with the content being the input string:
  QList<AST::TextNode*> textNodes = p.document()->childNodesByType<AST::TextNode>();
  QCOMPARE( textNodes.length(), 1 );
  QCOMPARE( textNodes.at( 0 )->text(), Template );
}

void ParserTest::simpleJavaScriptNode()
{
  const QByteArray Template = "<? var test = Math.max( 42, 1337 ); ?>";
  const QByteArray ExpectedScript = " var test = Math.max( 42, 1337 ); ";
  Parser p;
  
  // Parse must succeed:
  QVERIFY( p.parse( Template ) );
  
  // No error code:
  QCOMPARE( p.error(), Parser::NoError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // There must be exactly one child node:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  
  // There must be exactly one JavaScript child node, with the template
  // being the same as in the JavaScript string matching the ExpectedScript and
  // the print property being set to false:
  QList<AST::JavaScriptNode*> jsNodes = p.document()->childNodesByType<AST::JavaScriptNode>();
  QCOMPARE( jsNodes.length(), 1 );
  QCOMPARE( jsNodes.at( 0 )->javaScript(), ExpectedScript );
  QCOMPARE( jsNodes.at( 0 )->print(), false );
}

void ParserTest::simpleJavaScriptPrintedNode()
{
  const QByteArray Template = "${\"Hello World\"}";
  const QByteArray ExpectedScript = "\"Hello World\"";
  Parser p;
  
  // Parse must succeed:
  QVERIFY( p.parse( Template ) );
  
  // No error:
  QCOMPARE( p.error(), Parser::NoError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // There must be exactly one child node:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  
  // There must be exactly one JS child node with an expected content and printable:
  QList<AST::JavaScriptNode*> nodes = p.document()->childNodesByType<AST::JavaScriptNode>();
  QCOMPARE( nodes.length(), 1 );
  QCOMPARE( nodes.at( 0 )->javaScript(), ExpectedScript );
  QCOMPARE( nodes.at( 0 )->print(), true );
}

void ParserTest::simpleIncludeNode()
{
  const QByteArray Template = "<?include \"./fragment.qjst\"?>";
  const QByteArray expectedFileName = "./fragment.qjst";
  Parser p;
  
  // Parsing must fail:
  QTest::ignoreMessage( 
        QtWarningMsg, 
        QRegularExpression( "Found include statement but parser includes are disabled." ) );
  QVERIFY( !p.parse( Template ) );
  
  // We did not enable parser includes, so this should be given as error:
  QCOMPARE( p.error(), Parser::ParserIncludesDisabledError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // There must be exactly one child node:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  
  // There must be exactly one include child node with an expected content:
  QList<AST::IncludeNode*> nodes = p.document()->childNodesByType<AST::IncludeNode>();
  QCOMPARE( nodes.length(), 1 );
  QCOMPARE( nodes.at( 0 )->fileName(), expectedFileName );
}

void ParserTest::loadFromDevice()
{
  QByteArray Template = "Hello ${user.name}";
  Parser p;
  
  QBuffer buffer( &Template );
  QVERIFY( buffer.open( QIODevice::ReadOnly ) );

  // Loading from a QIODevice must succeed:  
  QVERIFY( p.parse( &buffer ) );
  
  // With no error:
  QCOMPARE( p.error(), Parser::NoError );
  
  // And the AST must exist:
  QVERIFY( p.document() != nullptr );
}

void ParserTest::loadFromFile()
{
  const QString SourceFile = SRCDIR "/main.qjst";
  Parser p;

  // This should fail as we have not enabled parser includes (used in the file):
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "Found include statement but parser includes are disabled." ) );
  QVERIFY( !p.parseFile( SourceFile ) );
  
  // Consequentially, we should get a "includes not enabled" error:
  QCOMPARE( p.error(), Parser::ParserIncludesDisabledError );

  // and the AST must exist:
  QVERIFY( p.document() != nullptr );
}

void ParserTest::complexExample()
{
  const QString SourceDir = SRCDIR;
  const QString MainFile = SRCDIR "/main.qjst";
  Parser p;
  
  // Enable includes:
  p.configuration()->enableParserOption( Configuration::EnableParserIncludes );
  p.configuration()->addParserIncludePath( SourceDir );
  
  // Parse the external file:
  QVERIFY( p.parseFile( MainFile ) );
  
  // The main fail must have a predefined structure:
  const AST::Document *root = p.document();
  QList<AST::Node*> childNodes = root->childNodes();
  QCOMPARE( childNodes.length(), 8 );
  QVERIFY( dynamic_cast< AST::IncludeNode* >( childNodes.at( 0 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::TextNode* >( childNodes.at( 1 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::JavaScriptNode* >( childNodes.at( 2 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::TextNode* >( childNodes.at( 3 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::JavaScriptNode* >( childNodes.at( 4 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::TextNode* >( childNodes.at( 5 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::JavaScriptNode* >( childNodes.at( 6 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::TextNode* >( childNodes.at( 7 ) ) != nullptr );
  
  // The include node must have a predefined structure:
  AST::IncludeNode *include = dynamic_cast<AST::IncludeNode*>( root->childNodes().at( 0 ) );
  QCOMPARE( include->childNodes().length(), 2 );
  QVERIFY( dynamic_cast< AST::JavaScriptNode*>( include->childNodes().at( 0 ) ) != nullptr );
  QVERIFY( dynamic_cast< AST::TextNode*>( include->childNodes().at( 1 ) ) != nullptr );
}

void ParserTest::openJavaScriptBlock()
{
  const QByteArray Template = "<? // This block is never closed - this is good for shared fragments!";
  Parser p;
  
  // Parsing must succeed:
  QVERIFY( p.parse( Template ) );
  
  // No error:
  QCOMPARE( p.error(), Parser::NoError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // And consist of exactly one JavaScript node:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  QCOMPARE( p.document()->childNodesByType<AST::JavaScriptNode>().length(), 1 );
}

void ParserTest::openJavaScriptPrintBlock()
{
  const QByteArray Template = "${\"This is an open JS printed block - good for shared fragments!\"";
  Parser p;
  
  // Parsing must succeed:
  QVERIFY( p.parse( Template ) );
  
  // No error:
  QCOMPARE( p.error(), Parser::NoError );
  
  // AST must exist:
  QVERIFY( p.document() != nullptr );
  
  // And consist of exactly one JavaScript node:
  QCOMPARE( p.document()->childNodes().length(), 1 );
  QCOMPARE( p.document()->childNodesByType<AST::JavaScriptNode>().length(), 1 );
}

void ParserTest::nonExistingFile()
{
  const QString fileName = SRCDIR "this_file_does_not_exist.qjst";
  Parser p;
  
  // Parsing must fail:
  QTest::ignoreMessage(
        QtWarningMsg,
        QRegularExpression( "iled to open .*: No such file or directory" ) );
  QVERIFY( !p.parseFile( fileName ) );
  
  // We should get an IO Error:
  QCOMPARE( p.error(), Parser::ParserIOError );
}

QTEST_APPLESS_MAIN(ParserTest)

#include "tst_parsertest.moc"
