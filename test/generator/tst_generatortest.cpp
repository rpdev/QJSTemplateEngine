#include <QString>
#include <QtTest>

#include "QJSTemplateEngine/AST"
#include "QJSTemplateEngine/Generator"

using namespace QJSTemplateEngine;

class GeneratorTest : public QObject
{
  Q_OBJECT
  
public:
  GeneratorTest();
  
private Q_SLOTS:
  void defaultConfiguration();
  void generateSimpleText();
  void generateExecutableJavaScript();
  void generatePrintableJavaScript();
  void generateComplexExample();
};

GeneratorTest::GeneratorTest()
{
}

void GeneratorTest::defaultConfiguration()
{
  Generator g;
  
  // Default configuration should be sane:
  QVERIFY( !g.configuration().isNull() );
}

void GeneratorTest::generateSimpleText()
{
  const QByteArray ExpectedResult = "QJSTemplateEngine.print(\"Hello World!\");\n";
  AST::Document root;
  AST::TextNode *textNode = root.createTextNode();
  QVERIFY( textNode != nullptr );
  textNode->setText( "Hello World!" );
  
  Generator g;
  QByteArray result = g.generate( &root );
  
  QCOMPARE( result, ExpectedResult );
}

void GeneratorTest::generateExecutableJavaScript()
{
  const QByteArray ExpectedResult = "var maxNum = Math.max( 42, 1337 );\n";
  AST::Document root;
  AST::JavaScriptNode *jsNode = root.createJavaScriptNode();
  QVERIFY( jsNode != nullptr );
  jsNode->setJavaScript( "var maxNum = Math.max( 42, 1337 );" );
  jsNode->setPrint( false );
  
  Generator g;
  QByteArray result = g.generate( &root );
  
  QCOMPARE( result, ExpectedResult );
}

void GeneratorTest::generatePrintableJavaScript()
{
  const QByteArray ExpectedResult = "QJSTemplateEngine.print(maxNum);\n";
  AST::Document root;
  AST::JavaScriptNode *jsNode = root.createJavaScriptNode();
  QVERIFY( jsNode != nullptr );
  jsNode->setJavaScript( "maxNum" );
  jsNode->setPrint( true );
  
  Generator g;
  QByteArray result = g.generate( &root );
  
  QCOMPARE( result, ExpectedResult );
}

void GeneratorTest::generateComplexExample()
{
  const QByteArray ExpectedResult = 
      "QJSTemplateEngine.print(\"Hello World\");\n"
      "var i = 42;\n"
      "QJSTemplateEngine.print(i);\n"
      "QJSTemplateEngine.print(\"Hi there\");\n"
      "i = 1337;\n"
      "QJSTemplateEngine.print(i);\n";
  AST::Document root;
  
  AST::TextNode *textNode = root.createTextNode();
  QVERIFY( textNode != nullptr );
  textNode->setText("Hello World");
  
  AST::JavaScriptNode *ejsNode = root.createJavaScriptNode();
  QVERIFY( ejsNode != nullptr );
  ejsNode->setJavaScript( "var i = 42;" );
  ejsNode->setPrint( false );
  
  AST::JavaScriptNode *pjsNode = root.createJavaScriptNode();
  QVERIFY( pjsNode != nullptr );
  pjsNode->setJavaScript( "i" );
  pjsNode->setPrint( true );
  
  AST::IncludeNode *includeNode = root.createIncludeNode();
  QVERIFY( includeNode != nullptr );
  
  AST::TextNode *subTextNode = includeNode->createTextNode();
  QVERIFY( subTextNode != nullptr );
  subTextNode->setText( "Hi there" );
  
  AST::JavaScriptNode *subEjsNode = includeNode->createJavaScriptNode();
  QVERIFY( subEjsNode != nullptr );
  subEjsNode->setJavaScript( "i = 1337;" );
  subEjsNode->setPrint( false );
  
  AST::JavaScriptNode *subPjsNode = includeNode->createJavaScriptNode();
  QVERIFY( subPjsNode != nullptr );
  subPjsNode->setJavaScript( "i" );
  subPjsNode->setPrint( true );
  
  Generator g;
  QByteArray result = g.generate( &root );
  
  QCOMPARE( result, ExpectedResult );
}

QTEST_APPLESS_MAIN(GeneratorTest)

#include "tst_generatortest.moc"
