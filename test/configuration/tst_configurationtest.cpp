#include <QString>
#include <QtTest>
#include <QMimeDatabase>

#include "QJSTemplateEngine/Configuration"
#include "QJSTemplateEngine/FileLoader"
#include "QJSTemplateEngine/PlainTextFileLoader"
#include "QJSTemplateEngine/QVariantDataProvider"

using namespace QJSTemplateEngine;

class ConfigurationTest : public QObject
{
  Q_OBJECT
  
public:
  ConfigurationTest();
  
private Q_SLOTS:
  void testMaxParserIncludeNextingLevel();
  void testParserIncludePaths();
  void testEngineIncludePaths();
  void testParserOptions();
  void testEngineOptions();
  void testRegisterFileLoader();
  void testPlainTextSupport();
};

class ConfigurationTestFileLoader : public FileLoader {

public:
  ConfigurationTestFileLoader(QObject *pare = 0);
  
  // FileLoader interface
  DataProvider *loadFile(const QString &fileName) const override;
};

ConfigurationTest::ConfigurationTest()
{
}

void ConfigurationTest::testMaxParserIncludeNextingLevel()
{
  Configuration configuration;
  // Default should be to have parser nesting limitations disabled:
  QVERIFY( configuration.maxParserIncludeNestingLevel() < 0 );
  
  // Settings the nesting level should result in a change:
  configuration.setMaxParserIncludeNestingLevel( 10 );
  QVERIFY( configuration.maxParserIncludeNestingLevel() == 10 );
}

void ConfigurationTest::testParserIncludePaths()
{
  Configuration configuration;
  const QString sampleDir = SRCDIR;
  
  // Default: Include path list is empty:
  QVERIFY( configuration.parserIncludePaths().isEmpty() );
  
  // Add a path and check that it gets added:
  bool result = configuration.addParserIncludePath( SRCDIR );
  QVERIFY( result == true );
  QVERIFY( configuration.parserIncludePaths().length() == 1 &&
           configuration.parserIncludePaths().contains( sampleDir ) );
  
  // Allow overriding
  configuration.setParserIncludePaths( QStringList() );
  QVERIFY( configuration.parserIncludePaths().isEmpty() );
}

void ConfigurationTest::testEngineIncludePaths()
{
  Configuration configuration;
  const QString sampleString = SRCDIR;
  
  // Default: Include path list empty:
  QVERIFY( configuration.engineIncludePaths().isEmpty() );
  
  // Add a path and check that it gets added:
  bool result = configuration.addEngineIncludePath( sampleString );
  QVERIFY( result == true );
  QVERIFY( configuration.engineIncludePaths().length() == 1 &&
           configuration.engineIncludePaths().contains( sampleString ) );
  
  // Allow overriding:
  configuration.setEngineIncludePaths( QStringList() );
  QVERIFY( configuration.engineIncludePaths().isEmpty() );
}

void ConfigurationTest::testParserOptions()
{
  Configuration configuration;
  
  // By default, all options should be disabled:
  QVERIFY( configuration.parserOptions() == Configuration::NoParserOption );
  
  // Enabling a single option should result in that specific option to get enabled:
  configuration.enableParserOption( Configuration::EnableParserIncludes );
  QVERIFY( configuration.parserOptions() == Configuration::EnableParserIncludes );
  
  // Disabling the flag should result in that flag to be disabled again:
  configuration.disableParserOption( Configuration::EnableParserIncludes );
  QVERIFY( configuration.parserOptions() == Configuration::NoParserOption );
  
  // Settings the parser options should result in that options to be applied:
  configuration.setParserOptions( Configuration::EnableParserIncludes );
  QVERIFY( configuration.parserOptions() == Configuration::EnableParserIncludes );
}

void ConfigurationTest::testEngineOptions()
{
  Configuration configuration;
  
  // By default, all options should be disabled:
  QVERIFY( configuration.engineOptions() == Configuration::NoEngineOption );
  
  // Test that enabling a single option works:
  configuration.enableEngineOption( Configuration::EnableEngineIncludes );
  QVERIFY( configuration.engineOptions() == Configuration::EnableEngineIncludes );
  
  // Test that disabling a single option works:
  configuration.disableEngineOption( Configuration::EnableEngineIncludes );
  QVERIFY( configuration.engineOptions() == Configuration::NoEngineOption );
  
  // Test that setting the options works:
  configuration.setEngineOptions( Configuration::EnableEngineIncludes );
  QVERIFY( configuration.engineOptions() == Configuration::EnableEngineIncludes );
}

void ConfigurationTest::testRegisterFileLoader()
{
  Configuration configuration;
  
  // Register a new file loader:
  ConfigurationTestFileLoader *loader = new ConfigurationTestFileLoader();
  Q_CHECK_PTR( loader );
  
  configuration.registerFileLoader( loader );
  
  // The configuration should have taken ownership of the object:
  QVERIFY2( &configuration == loader->parent(), 
            "Configuration does not own registered FileLoader!");
  
  // Using the name of the FileLoader, is should be possible to query it:
  QVERIFY2( configuration.fileLoaderByName( "TestCase" ) == loader,
            "Unable to retrieve registered FileLoader by name!" );
  
  // Using a file name with *.test suffix, it should be possible to
  // query the loader:
  QVERIFY2( configuration.fileLoaderByFileNameSuffix( "test" ) == loader,
            "Unable to retrieve registered FileLoader by suffix!" );
  
  // Make sure also a secondary suffix is considered:
  QVERIFY2( configuration.fileLoaderByFileNameSuffix( "tst" ) == loader,
            "Unable to retrieve registered FileLoader by secondary suffix!" );
  
  // Make sure we can retrieve a loader by MIME type from file:
  QVERIFY2( configuration.fileLoaderByMimeType( ":/test.dat" ) == loader,
            "Unable to retrieve registered FileLoader by MIME type from file" );
  
  // Make sure we can retrieve a loader by MIME type from QFileInfo:
  QFileInfo fi( ":/test.dat" );
  QVERIFY2( configuration.fileLoaderByMimeType( fi ) == loader,
            "Unable to retrieve registered FileLoader by MIME type from QFileInfo" );
}

void ConfigurationTest::testPlainTextSupport()
{
  Configuration configuration;
  
  // Make sure we can handle plain text files:
  const FileLoader *loader = configuration.fileLoaderByMimeType( ":/test.txt" );
  Q_CHECK_PTR( loader );
  QVERIFY2( loader->name() == PlainTextFileLoader::Name,
            "Unexpected loader returned handling text/plain files!");
  
  DataProvider *data = loader->loadFile( ":/test.txt" );
  Q_CHECK_PTR( data );
  delete data;
}


ConfigurationTestFileLoader::ConfigurationTestFileLoader( QObject *parent ) :
  FileLoader( 
    "TestCase", 
    { "test", "tst" },
    { QMimeDatabase().mimeTypeForName("application/octet-stream") }, 
    parent )
{
}

DataProvider *ConfigurationTestFileLoader::loadFile(const QString &fileName) const
{
  return new QVariantDataProvider( fileName );
}

QTEST_APPLESS_MAIN(ConfigurationTest)

#include "tst_configurationtest.moc"
