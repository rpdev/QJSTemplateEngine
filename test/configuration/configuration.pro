include(../../config.pri)
setupUnitTest(configuration)

QT += qml

SOURCES += tst_configurationtest.cpp

DISTFILES += \
    test.txt

RESOURCES += \
    resources.qrc
