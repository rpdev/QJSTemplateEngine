TEMPLATE = subdirs

SUBDIRS += \
    configuration \
    parser \
    generator \
    engine \
    qvariantdataprovider \
    plaintextfileloader \
    jsonfileloader \
    inifileloader
