#include <QString>
#include <QtTest>

#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/QVariantDataProvider"

using namespace QJSTemplateEngine;

class QvariantdataproviderTest : public QObject
{
  Q_OBJECT
  
public:
  QvariantdataproviderTest();
  
private Q_SLOTS:
  void testScalarValue();
  void testListValue();
  void testMapValue();
};

QvariantdataproviderTest::QvariantdataproviderTest()
{
}

void QvariantdataproviderTest::testScalarValue()
{
  const QByteArray Template = "Hello Mr. ${QJSTemplateEngine.data.name}!";
  const QByteArray ExpectedOUtput = "Hello Mr. World!";

  QVariant name = "World";
  
  QByteArray output;
  Engine engine;
  engine.addDataProvider( "name", new QVariantDataProvider( name, &engine ) );
  QVERIFY2( engine.render( Template, output ), "Rendering failed" );
  QCOMPARE( output, ExpectedOUtput );
}

void QvariantdataproviderTest::testListValue()
{
  const QByteArray Template = 
      "<ul><? var list = QJSTemplateEngine.data.list;"
      "for ( var i = 0; i < QJSTemplateEngine.data.list.length; ++i ) {?>"
      "<li>${list[i]}</li>"
      "<?}?></ul>";
  const QByteArray ExpectedResult = "<ul><li>Entry 1</li><li>Entry 2</li><li>Entry 3</li></ul>";
  
  QVariant list = QStringList( { "Entry 1", "Entry 2", "Entry 3" } );
  
  QByteArray output;
  Engine engine;
  engine.addDataProvider( "list", new QVariantDataProvider( list, &engine ) );
  QVERIFY2( engine.render( Template, output ), "Rendering failed" );
  QCOMPARE( output, ExpectedResult );
}

void QvariantdataproviderTest::testMapValue()
{
  const QByteArray Template = "Hello Mr. ${QJSTemplateEngine.data.person.name} ${QJSTemplateEngine.data.person.surname}";
  const QByteArray ExpectedResult = "Hello Mr. Foo Bar";
  
  QVariantMap map;
  map.insert( "name", "Foo" );
  map.insert( "surname", "Bar" );
  
  QByteArray output;
  Engine engine;
  engine.addDataProvider( "person", new QVariantDataProvider( map, &engine ) );
  QVERIFY2( engine.render( Template, output ), "Rendering failed" );
  QCOMPARE( output, ExpectedResult );
}

QTEST_APPLESS_MAIN(QvariantdataproviderTest)

#include "tst_qvariantdataprovidertest.moc"
