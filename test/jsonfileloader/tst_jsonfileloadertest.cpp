#include "QJSTemplateEngine/Configuration"
#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/FileLoader"
#include "QJSTemplateEngine/JSONFileLoader"

#include <QFile>
#include <QString>
#include <QtTest>

using namespace QJSTemplateEngine;

class JSONFileLoaderTest : public QObject
{
  Q_OBJECT
  
public:
  JSONFileLoaderTest();
  
private Q_SLOTS:
  void jsonSupportedByDefault();
  void openJSONByMime();
  void openJSONByFileNameExtension();
  void jsonStructure();
};

JSONFileLoaderTest::JSONFileLoaderTest()
{
}

void JSONFileLoaderTest::jsonSupportedByDefault()
{
  Configuration configuration;
  const FileLoader *loader = configuration.fileLoaderByFileNameSuffix( "json" );
  Q_CHECK_PTR( loader );
  QCOMPARE( loader->name(), JSONFileLoader::Name ); 
}

void JSONFileLoaderTest::openJSONByMime()
{
  Engine engine;
  QVERIFY2( engine.addFile( SRCDIR "/data.json" ),
            "Cannot load JSON by MIME type" );
}

void JSONFileLoaderTest::openJSONByFileNameExtension()
{
  Engine engine;
  QFileInfo fi( SRCDIR "/data.json" );
  QVERIFY2( engine.addFileBySuffix( SRCDIR "/data.json" ),
            "Cannot load JSON by file name extension" );
}

void JSONFileLoaderTest::jsonStructure()
{
  Engine engine;
  QVERIFY2( engine.addFile( SRCDIR "/data.json" ), 
            "Failed to load data.json" );
  QByteArray output;
  QVERIFY2( engine.renderFile( SRCDIR "/template.qjst", output ),
            "Failed to render template" );
  QFile file( SRCDIR "/expected_output.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ),
            "Failed to load reference data" );
  QCOMPARE( output, file.readAll() );
}


QTEST_APPLESS_MAIN(JSONFileLoaderTest)

#include "tst_jsonfileloadertest.moc"
