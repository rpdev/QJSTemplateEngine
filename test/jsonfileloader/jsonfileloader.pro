include(../../config.pri)
setupUnitTest(jsonfileloader)

SOURCES += tst_jsonfileloadertest.cpp

DISTFILES += \
    data.json \
    expected_output.txt \
    template.qjst

