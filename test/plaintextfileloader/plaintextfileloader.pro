include(../../config.pri)
setupUnitTest(plaintextfileloader)

SOURCES += tst_plaintextfileloadertest.cpp

DISTFILES += \
    test.txt \
    identity.qjst

RESOURCES += \
    resources.qrc
