#include "QJSTemplateEngine/Engine"
#include "QJSTemplateEngine/PlainTextFileLoader"

#include <QString>
#include <QtTest>
#include <QFile>
#include <QBuffer>

using namespace QJSTemplateEngine;

class PlainTextFileLoaderTest : public QObject
{
  Q_OBJECT
  
public:
  PlainTextFileLoaderTest();
  
private Q_SLOTS:
  void testPlainTextSupportedByDefault();
  void testIdentityTextTransformation();
};

PlainTextFileLoaderTest::PlainTextFileLoaderTest()
{
}

void PlainTextFileLoaderTest::testPlainTextSupportedByDefault()
{
  Configuration configuration;
  const FileLoader *loader = configuration.fileLoaderByMimeType(
        ":/test.txt" );
  Q_CHECK_PTR( loader );
  QCOMPARE( loader->name(), PlainTextFileLoader::Name );
}

void PlainTextFileLoaderTest::testIdentityTextTransformation()
{
  Engine engine;
  QVERIFY2( engine.addFile( ":/test.txt" ),
            "Failed to load test.txt" );
  
  // Load input data - should be equal to what gets generated
  QFile file( ":/test.txt" );
  QVERIFY2( file.open( QIODevice::ReadOnly ),
            "Failed to open test.txt" );
  QByteArray reference = file.readAll();
  
  //Render to buffer:
  QByteArray output;
  engine.renderFile( ":/identity.qjst", output );
  
  QCOMPARE( output, reference );
}

QTEST_APPLESS_MAIN(PlainTextFileLoaderTest)

#include "tst_plaintextfileloadertest.moc"
