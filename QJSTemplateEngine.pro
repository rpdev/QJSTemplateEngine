TEMPLATE = subdirs
SUBDIRS = \
    src \
    test

CONFIG += ordered

OTHER_FILES += \
    README.md \
    COPYING \
    COPYING.LESSER \
    LICENSE
